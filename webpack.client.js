const webpack = require('webpack');
const path = require('path')
const autoprefixer = require("autoprefixer");

module.exports = {
    devtool: 'cheap-module-source-map',
    entry: './src/client.js',
    output: {
        filename: 'client_bundle.js',
        path: path.resolve(__dirname, 'public'),
        publicPath: '/public'
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx|mjs)$/,
                loader: 'babel-loader',
                exclude: '/node_modules/'
            },
            {
                test: /\.(s*)css$/,
                use: [
                    {
                        loader: "isomorphic-style-loader"
                    },
                    {
                        loader: "css-loader",
                        options: {
                            module: true,
                            importLoaders: 1,
                            sourceMap: true,
                            minimize: true
                        }
                    },
                    {
                        loader: require.resolve("postcss-loader"),
                        options: {
                            // Necessary for external CSS imports to work
                            // https://github.com/facebookincubator/create-react-app/issues/2677
                            ident: "postcss",
                            plugins: () => [
                                require("postcss-flexbugs-fixes"),
                                autoprefixer({
                                    browsers: [
                                        ">1%",
                                        "last 4 versions",
                                        "Firefox ESR",
                                        "not ie < 9" // React doesn't support IE8 anyway
                                    ],
                                    flexbox: "no-2009"
                                })
                            ],
                            sourceMap: true
                        }
                    },
                    {
                        loader: "sass-loader",
                        options: {
                            sourceMap: true
                        }
                    }
                ]
            },
            {
                test: /\.(jpe?g|png|gif|svg|ttf)$/i,
                loaders: [
                    'url?limit=8192',
                    'img'
                ]
            }
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            "process.env": {
                BROWSER: JSON.stringify(true)
            }
        })
    ]
}