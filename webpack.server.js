const path = require('path')
const autoprefixer = require("autoprefixer");
const webpackNodeExternals = require('webpack-node-externals')

module.exports = {
    target: 'node',
    entry: './serve.js',
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'server_build'),
        publicPath: '/server_build'
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx|mjs)$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel-loader'
            },
            {
                test: /\.(s*)css$/,
                use: [
                    {
                        loader: 'isomorphic-style-loader',
                    },
                    {
                        loader: "css-loader",
                        options: {
                            module: true,
                            importLoaders: 1,
                            sourceMap: true,
                            minimize: true
                        }
                    },
                    {
                        loader: require.resolve("postcss-loader"),
                        options: {
                            // Necessary for external CSS imports to work
                            // https://github.com/facebookincubator/create-react-app/issues/2677
                            ident: "postcss",
                            plugins: () => [
                                require("postcss-flexbugs-fixes"),
                                autoprefixer({
                                    browsers: [
                                        ">1%",
                                        "last 4 versions",
                                        "Firefox ESR",
                                        "not ie < 9" // React doesn't support IE8 anyway
                                    ],
                                    flexbox: "no-2009"
                                })
                            ],
                            sourceMap: true
                        }
                    },
                    {
                        loader: "sass-loader",
                        options: {
                            sourceMap: true
                        }
                    }
                ]
            }
        ]
    },
    externals: [webpackNodeExternals()]
}
