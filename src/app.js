import React, { Component } from "react";
import withStyles from 'isomorphic-style-loader/lib/withStyles'
import s from "./scss/app.scss";
import Main from "./pages";


class App extends Component {

    render() {
        return (
            <div className="App">
                <Main {...this.props} />
            </div>
        );
    }
}

export default withStyles(s)(App);
