import React from "react"
import ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import ContextProvider from './ContextProvider.js'
import App from './app'

const context = {
    insertCss: (...styles) => {
        const removeCss = styles.map(x => x._insertCss());
        return () => {
            removeCss.forEach(f => f());
        };
    },
}

ReactDOM.hydrate(
    <BrowserRouter>
        <ContextProvider context={context}>
            <App />
        </ContextProvider>
    </BrowserRouter>,
    document.querySelector('#app')
)