import React, { Component } from "react";
import { Route, Switch, withRouter } from "react-router-dom";
import VestedLayout from "./layout/vestedLayout"
import BrandingLayout from "./layout/brandingLayout"
import CompanyAdminLayout from "./layout/companyAdminLayout"
import Profile from "./profile"
class Main extends Component {

    state = {
        loading: false
    }
    componentDidMount() {
        this.setState({
            loading: true
        })

        setTimeout(() => {
            this.setState({ loading: false })
        }, 1000, this)
    }
    componentDidUpdate(prevProps) {
        if (this.props.location.pathname !== prevProps.location.pathname) {
            window.scrollTo(0, 0)
            this.setState({
                loading: true
            })

            setTimeout(() => {
                this.setState({ loading: false })
            }, 1000, this)
        }
    }
    render() {
        return (
            <main>
                {this.state.loading && <div className="lds-wrapper"><div class="lds-ring">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
                </div>}
                <Switch>
                    <Route path="/moka" render={() => <BrandingLayout {...this.props} />} />
                    <Route path="/endeavor" render={() => <BrandingLayout {...this.props} />} />
                    <Route path="/company-admin" render={() => <CompanyAdminLayout {...this.props} />} />
                    <Route path="/profile" render={() => <Profile {...this.props} />} />
                    <Route path="/" render={() => <VestedLayout {...this.props} />} />
                </Switch>


            </main>
        )
    }
};

export default withRouter(Main);
