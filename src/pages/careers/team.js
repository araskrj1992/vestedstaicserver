import React from "react";
import { Row, Col } from "react-bootstrap";
const Team = () => {
    return (
        <section className="team" >
            <Row>
                <Col xs={12} md={12} className="text-center">
                    <h2 className="heading">Meet Our Team</h2>
                </Col>
            </Row>
            <Row className="content-wrapper">
                <Col xs={12} sm={6} md={4}>
                    <div className="team-content">
                        <img src="/images/careers/akash.png" alt="akash srivastava vested" />
                        <div className="overlay">
                            <div className="member-details">
                                <h3>Akash Srivastava</h3>
                                {/* <h4>C.E.O</h4>
                                <p>“We’re always looking for the best and the brightest”</p> */}
                                <a href="https://www.linkedin.com/in/asrivastava02/" target="_blank" rel="noopener noreferrer"><span className="social-wrapper"><img src="/images/careers/linkedin.svg" alt="linkedin" /></span></a>
                            </div>
                        </div>
                    </div>
                </Col>
                <Col xs={12} sm={6} md={4}>
                    <div className="team-content">
                        <img src="/images/careers/andy.png" alt="andy seidl vested" />
                        <div className="overlay">
                            <div className="member-details">
                                <h3>Andy Seidl</h3>
                                {/* <h4>C.E.O</h4>
                                <p>“We’re always looking for the best and the brightest”</p> */}
                                <a href="https://www.linkedin.com/in/seidland/" target="_blank" rel="noopener noreferrer"><span className="social-wrapper"><img src="/images/careers/linkedin.svg" alt="linkedin" /></span></a>
                            </div>
                        </div>
                    </div>
                </Col>
                <Col xs={12} sm={6} md={4}>
                    <div className="team-content">
                        <img src="/images/careers/reggie.png" alt="reggie beltran vested" />
                        <div className="overlay">
                            <div className="member-details">
                                <h3>Reggie Beltran</h3>
                                {/* <h4>C.E.O</h4>
                                <p>“We’re always looking for the best and the brightest”</p> */}
                                <a href="https://www.linkedin.com/in/reginald-beltran-0545827" target="_blank" rel="noopener noreferrer"><span className="social-wrapper"><img src="/images/careers/linkedin.svg" alt="linkedin" /></span></a>
                            </div>
                        </div>
                    </div>
                </Col>
                <Col xs={12} sm={6} md={4}>
                    <div className="team-content">
                        <img src="/images/careers/monqiue.png" alt="Monqiue Cordova vested" />
                        <div className="overlay">
                            <div className="member-details">
                                <h3>Monqiue Cordova</h3>
                                {/* <h4>C.E.O</h4>
                                <p>“We’re always looking for the best and the brightest”</p> */}
                                <a href="https://www.linkedin.com/in/moniquecordova/" target="_blank" rel="noopener noreferrer"><span className="social-wrapper"><img src="/images/careers/linkedin.svg" alt="linkedin" /></span></a>
                            </div>
                        </div>
                    </div>
                </Col>
                <Col xs={12} sm={6} md={4}>
                    <div className="team-content">
                        <img src="/images/careers/ankit.png" alt="ankit gupta vested" />
                        <div className="overlay">
                            <div className="member-details">
                                <h3>Ankit Gupta</h3>
                                {/* <h4>C.E.O</h4>
                                <p>“We’re always looking for the best and the brightest”</p> */}
                                <a href="https://www.linkedin.com/in/ankit-gupta-b95815a9/" target="_blank" rel="noopener noreferrer"><span className="social-wrapper"><img src="/images/careers/linkedin.svg" alt="linkedin" /></span></a>
                            </div>
                        </div>
                    </div>
                </Col>
                <Col xs={12} sm={6} md={4}>
                    <div className="team-content">
                        <img src="/images/careers/amber.png" alt="amber varma vested" />
                        <div className="overlay">
                            <div className="member-details">
                                <h3>Amber Varma</h3>
                                {/* <h4>C.E.O</h4>
                                <p>“We’re always looking for the best and the brightest”</p> */}
                                <a href="https://www.linkedin.com/in/ambervarma/" target="_blank" rel="noopener noreferrer"><span className="social-wrapper"><img src="/images/careers/linkedin.svg" alt="linkedin" /></span></a>
                            </div>
                        </div>
                    </div>
                </Col>
            </Row>
        </section>
    );
}


export default Team;
