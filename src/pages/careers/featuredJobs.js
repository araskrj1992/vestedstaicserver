import React from "react";
import { Row, Col } from "react-bootstrap";
const FeaturedJobs = () => {
    return (
        <section className="featuredJobs" id="featuredJobs">
            <Row>
                <Col xs={12} md={12} className="text-center">
                    <h2 className="heading">Featured Jobs</h2>
                    <p className="sub-head">Check out our openings and apply today!</p>
                </Col>
            </Row>
            <Row className="content-wrapper">
                <Col xs={12} md={6}>
                    <div className="job-content">
                        <h4 className="job-name">business development (sales)<span><img src="/images/careers/location.svg" alt="new york" />New York City, NY</span></h4>
                        {/* <h5 className="company-name">Tesla Motors</h5> */}
                        <p className="description">Business Development is all about helping businesses grow!  This is a great entry point into an organization.</p>
                        <div className="btn-wrapper">
                            <a
                                className="btn btn-sign-up"
                                href="https://hire.withgoogle.com/public/jobs/getvestedio/view/P_AAAAAADAAGhA5ccxvE0bNd"
                                target="_blank"
                                rel="noopener noreferrer"
                            >apply
                        </a>
                        </div>
                    </div>
                </Col>
                <Col xs={12} md={6}>
                    <div className="job-content">
                        <h4 className="job-name">Software Engineer (Back-End)<span><img src="/images/careers/location.svg" alt="new york" />New York City, NY</span></h4>
                        {/* <h5 className="company-name">Tesla Motors</h5> */}
                        <p className="description">This is a back-end dominant job.  You'll be helping Vested integrate with various resources, architect and build spec for new product.</p>
                        <div className="btn-wrapper">
                            <a
                                className="btn btn-sign-up"
                                href="https://hire.withgoogle.com/public/jobs/getvestedio/view/P_AAAAAADAAGhKh5TFTeavXa"
                                target="_blank"
                                rel="noopener noreferrer"
                            >apply
                        </a>
                        </div>
                    </div>
                </Col>
            </Row>
            <Row className="content-wrapper">
                <Col xs={12} md={6}>
                    <div className="job-content">
                        <h4 className="job-name">Product manager<span><img src="/images/careers/location.svg" alt="new york" />New York City, NY</span></h4>
                        {/* <h5 className="company-name">Tesla Motors</h5> */}
                        <p className="description">You will be an integral part of Vested's growth by conducting market research, understanding current products, and helping to develop new ones.</p>
                        <div className="btn-wrapper">
                            <a
                                className="btn btn-sign-up"
                                href="https://hire.withgoogle.com/public/jobs/getvestedio/view/P_AAAAAADAAGhDHmmk7keJJn"
                                target="_blank"
                                rel="noopener noreferrer"
                            >apply
                        </a>
                        </div>
                    </div>
                </Col>
                <Col xs={12} md={6}>
                    <div className="job-content">
                        <h4 className="job-name">Corporate Recruiter<span><img src="/images/careers/location.svg" alt="new york" />New York City, NY</span></h4>
                        {/* <h5 className="company-name">Tesla Motors</h5> */}
                        <p className="description">Vested is all about talent, both for our customers and internally.  In this role you'll help us recruit internally for all positions.</p>
                        <div className="btn-wrapper">
                            <a
                                className="btn btn-sign-up"
                                href="https://hire.withgoogle.com/public/jobs/getvestedio/view/P_AAAAAADAAGhD3o8ZQy0kiP"
                                target="_blank"
                                rel="noopener noreferrer"
                            >apply
                        </a>
                        </div>
                    </div>
                </Col>
            </Row>
        </section>
    );
}


export default FeaturedJobs;
