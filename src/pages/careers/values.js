import React from "react";
import { Row, Col } from "react-bootstrap";
const OurValues = () => {
    return (
        <section className="ourValues">
            <Row>
                <Col xs={12} md={12} className="text-center">
                    <h2 className="heading">Our Values</h2>
                </Col>
            </Row>
            <Row className="content-wrapper">
                <Col xs={12} sm={6} md={4}>
                    <div className="value-content">
                        <div className="img-wrapper">
                            <img src="/images/careers/honest.svg" alt="honest" />
                        </div>
                        <h4 className="value-name">OPEN AND HONEST</h4>
                        <p className="description">We believe honesty and transparency build trust. We foster input from everyone.</p>
                    </div>
                </Col>
                <Col xs={12} sm={6} md={4}>
                    <div className="value-content">
                        <div className="img-wrapper">
                            <img src="/images/careers/accountability.svg" alt="accountability" />
                        </div>
                        <h4 className="value-name">PERSONAL ACCOUNTABILITY</h4>
                        <p className="description">We’re a fast-moving company focused on innovation which means our team is self-disciplined and accountable.</p>
                    </div>
                </Col>
                <Col xs={12} sm={6} md={4}>
                    <div className="value-content">
                        <div className="img-wrapper">
                            <img src="/images/careers/possible.svg" alt="possible" />
                        </div>
                        <h4 className="value-name">Assume Everything Is Possible</h4>
                        <p className="description">Our team dreams big and believes we can accomplish anything we set our minds to.</p>
                    </div>
                </Col>
                <Col xs={12} sm={6} md={4}>
                    <div className="value-content">
                        <div className="img-wrapper">
                            <img src="/images/careers/simplify.svg" alt="simplify" />
                        </div>
                        <h4 className="value-name">Simplify: Less Is More</h4>
                        <p className="description">We’re a team keen on efficiency. We strive for elegant and simple solutions to the most complex problems</p>
                    </div>
                </Col>
                <Col xs={12} sm={6} md={4}>
                    <div className="value-content">
                        <div className="img-wrapper">
                            <img src="/images/careers/learning.svg" alt="learning" />
                        </div>
                        <h4 className="value-name">Always Be Learning</h4>
                        <p className="description">Curiosity and knowledge are at the heart of our company. We empower our team to continuously develop themselves and others.</p>
                    </div>
                </Col>
                <Col xs={12} sm={6} md={4}>
                    <div className="value-content">
                        <div className="img-wrapper">
                            <img src="/images/careers/people.svg" alt="people" />
                        </div>
                        <h4 className="value-name">The Best People</h4>
                        <p className="description">People are our most important asset. We strive to attract the best talent so we can be the best company.</p>
                    </div>
                </Col>
            </Row>
        </section >
    );
}


export default OurValues;
