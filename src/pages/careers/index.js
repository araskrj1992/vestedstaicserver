import React, { Fragment } from "react";
import WhatWho from "../common/whatWho";
import Splash from "../common/splash";
import Meta from "../common/meta";
import OurValues from "./values"
import FeaturedJobs from "./featuredJobs"
import FrequentQuestions from "../common/frequentQuestions"
import Team from "./team"



const questions = [
    {
        title: "What are the normal working hours at Vested?",
        answer: "Vested believes in the power of autonomy, independence and trust.  Our employees are welcome to work from home, take time off, and create their own schedules as long as expectations are being met."
    },
    {
        title: "What does Vested look for in employees?",
        answer: "We look for passionate, accountable and fun loving people that want to help us take Vested to the next level."
    },
    {
        title: "Does the company support remote positions?",
        answer: "Yes!  Vested is committed to hiring the best person for the job, regardless of geography."
    }
]


const Careers = () => {
    let metaTitle = 'Vested Technology';
    let metaDescription = 'Join the Recruiting Revolution';
    let metaImage = window.location.origin + '/images/og.png';
    return (
        <Fragment>
            <section className="jobs">
                <Meta title={metaTitle} description={metaDescription} image={metaImage} />
                <Splash
                    custClass="careers-splash"
                    heading="Find Fulfilling Work At Vested"
                    content="We’re a small group of fun, energetic, and passionate people set on making a difference. If you’re humble, creative, and love solving big problems we hope to hear from you!"
                    buttonText="View Jobs"
                    btnStyle="primary"
                    bgImg="/images/careers/splash.png"
                    scrollId="featuredJobs"
                />
                <WhatWho
                    heading="About Us"
                    text="Vested Technology is the first fully automated recruitment solution offering deep candidate insights.  Founded in 2016, we help job seekers find fulfilling work and companies make better hiring decisions.  We’re a small and diverse team out to make a big impact.  If you like solving problems and building businesses Vested could be the perfect fit!"
                    dark={true}
                />
                <OurValues />
                {/* <div className="companies">
                    <div className="companies-strip careers-companies" />
                </div> */}
                <img src="/images/careers/value.png" alt="our values" />
                <Team />
                <div className="benefits">
                    <h2 className="heading">Benefits And Perks</h2>
                    <p>Vested has a unique startup culture focused on teamwork and collaboration.  In addition to excellent healthcare and in-office snacks, Vested grants employees 1 day per quarter to volunteer in their community and 1 month paid leave after 2 years.  We’re also heavily focused on employee development.</p>
                </div>
                <FeaturedJobs />
                <FrequentQuestions questions={questions} />
                <section className="press-features">
                    <h2 className="heading">Recent Press</h2>
                    <div className="content-wrapper">
                        <div className="img-wrapper">
                            <img src="/images/careers/techcrunch.png" alt="techcrunch" style={{ maxWidth: '242px' }} />
                            <img src="/images/careers/verge.png" alt="verge" style={{ maxWidth: '155px' }} />
                            <img src="/images/careers/yourstory.png" alt="yourstory" style={{ maxWidth: '208px' }} />
                            <img src="/images/careers/newyorktimes.png" alt="newyorktimes" style={{ maxWidth: '234px' }} />
                        </div>
                    </div>
                </section>
            </section>
        </Fragment>
    );
};

export default Careers;



