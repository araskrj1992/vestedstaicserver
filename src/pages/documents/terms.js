import React from "react";

const Terms = () => {
    return (
        <section className="terms">
            <h1>Terms of Service</h1>

            <h2>Welcome</h2>
            <p>
                Welcome to Vested, (Vested Technology LLC, or as used herein: “Vested”, “We”, “Our”, or “Us”). When You (“You” individually or the entity that You represent) use our website (the “Site”) or the Vested recruiting service (collectively, the “Service”), You are agreeing to our Terms of Service (the “Agreement”) outlined below.
				</p>
            <p>
                By accessing or using this site in any way, including using the service, completing registration, and/or browsing the site, You acknowledge i) You have read and understand the Agreement, ii) You agree to be bound by this Agreement and any additional terms referenced herein, including our applicable privacy policy, iii) You have the authority to enter into the Agreement personally or on behalf of the entity You represent, and iv) You are over the age of 18.
				</p>
            <p>
                Throughout this document, the term “Candidate” means users who are seeking employment and/or contractor opportunities through Vested. The term “Client” means a company or individual interested in hiring Candidates.
				</p>
            <p>
                PLEASE NOTE THAT THIS AGREEMENT IS SUBJECT TO CHANGE BY Vested AT ANY TIME. When changes are made, Vested will upload a copy of the Terms of Service on the website which will include the “Last Revised” date. We may occasionally update the Terms of Service based on changes to the Site, among other factors. Any updates made to the Terms go into effect once they are posted – and by using the Site, you’re agreeing to the Terms.
				</p>

            <h2>Registration</h2>
            <p>
                In registering a profile or account with Vested, You agree to (1) provide true, accurate, current, and complete information about yourself as prompted by the registration form (“Registration Data”), and (2) maintain and promptly update the Registration Data to keep it true, accurate, and complete. You are responsible for all activities that occur under your account. If You provide false information, or if Vested has reason to believe your information is untrue, inaccurate, or not current, Vested has the right to suspend or terminate your account. This includes the right to refuse any and all current or future use of the Site and/or Service. You agree not to create an account using a false identity or information, or on behalf of someone other than yourself. You agree that You shall not have more than one account. You agree not to create an account if You have been previously removed or banned from Vested’s site or services. You understand that conducting yourself in a way that gets Vested in legal trouble may also result in litigation against you.
				</p>

            <h2>User Responsibilities</h2>
            <ul>
                <li>
                    You will not copy, distribute or disclose any part of the Site or the Service in any medium, including any automated or non-automated “scraping”
					</li>
                <li>
                    You will not attempt to interfere with, compromise site security, or decipher any transmissions the servers running the Site or Service
					</li>
                <li>
                    You will not upload invalid data, viruses, worms or other software agents through the Site and/or Service
					</li>
                <li>
                    You will not collect or harvest any personally identifiable information, including account names, email addresses, or content for any purpose other than the intended use of the Site and/or Service
					</li>
                <li>
                    You will not transfer, sell, or reassign the Service, to any third party, or offer the Service to other companies (or people outside of your company) on time-sharing or other commercial basis
					</li>
                <li>
                    You will not disclose the names or identities of Candidates found on Vested, outside of your recruiting department, hiring department, or company.
					</li>
                <li>You agree not to stalk, harass, bully or harm other individuals or companies</li>
                <li>
                    You agree not to impersonate any person or entity, use a fictitious name, or misrepresent your affiliation with a person or entity, which includes hacking, spamming or phishing Vested or users of Vested
					</li>
                <li>
                    You agree to not violate any law (local, state, federal or other) and You understand that You are responsible and liable for such violations
					</li>
                <li>
                    You will not use or upload content to our site that includes a third party intellectual property which You do not have rights over
					</li>
                <li>
                    You will use information found on Vested such as Candidate or Client Information for the sole purpose of recruiting, job seeking, and content consumption/sharing
					</li>
                <li>
                    The Candidate agrees not to attempt to circumvent our Site and Service by independently attempting to communicate with an Client that contacted or communicated with him or her on our Site or Service
					</li>
                <li>
                    The Client agrees not to attempt to circumvent our Site and Service by independently attempting to communicate and hire the Candidate through alternative means after discovering the Candidate on our Site or Service
					</li>
                <li>
                    Client agrees not to discriminate against employees or job applicants based on race, color, religion, sex, national origin, veteran status or disability
					</li>
            </ul>

            <h2>Vested Liability</h2>
            <p>
                Any agreements created between a Client and a Candidate are not binding by Vested. We are not liable for, or obligated to enforce, any agreements between a Client and a Candidate. We are not responsible for any damages whatsoever that arise as a result of contacting, interviewing, or hiring a Candidate. We are not responsible for any damages that arise as a result of contacting, being interviewed by, or hired by an Client.
				</p>
            <p>
                We are not required to or under any obligation to review, screen, edit, monitor, or remove any content posted on our Site, although we reserve the absolute right to remove, screen, or edit any content at any time and for any reason without any notice to You or any party affected
				</p>

            <h2>Misuse and Termination</h2>
            <p>
                Vested reserves the right to terminate any agreement, including Client or Candidate profiles, all data associated with any Client or Candidate, and communication on the platform at anytime without explanation or notice. We reserve the right to refuse re-entry to the platform for an indefinite period of time. If the Client and/or any person or entity acting on behalf of the Client are caught trying to circumvent the process, or a hire is made by intentionally circumventing the process, Vested reserves the right to go after compensation which may include legal action. Vested also reserves the right to audit hires we deem suspicious and that may have circumvented the process. This can include contacting the Client and/or new hire.
				</p>
            <p>
                YOU UNDERSTAND THAT VESTED DOES NOT INQUIRE INTO THE BACKGROUNDS OF ITS USERS OR ATTEMPT TO VERIFY THE STATEMENTS OF ITS USERS (ALTHOUGH IT RESERVES THE RIGHT TO CONDUCT BACKGROUND CHECKS OR OTHER SCREENINGS USING AVAILABLE PUBLIC RECORDS). YOU AGREE TO (1) CONDUCT ANY BACKGROUND CHECKS, REFERENCE CHECKS, OR OTHER DUE DILIGENCE BEFORE MAKING AN EMPLOYMENT OFFER TO A CANDIDATE, AND (2) COMPLY WITH ALL LAWS AND REGULATIONS RELATING TO THE INTENDED EMPLOYMENT OF ANY CANDIDATE. WE RESERVE THE RIGHT, IN OUR SOLE DISCRETION, TO ACCEPT OR REJECT YOUR REGISTRATION TO USE OUR SITE AND SERVICE.
				</p>

            <h2>Fee Arrangement</h2>
            <h3>For Candidates</h3>
            <p>
                Vested is free for Candidates. A Candidate is required to promptly notify Vested if he / she accepts an offer of employment (contract or fulltime) which originated on the Vested Platform. Notification includes a copy of the offer, start date, and compensation.
				</p>
            <h3>For Clients</h3>
            <p>
                Once Vested has accepted the Client’s registration, the Client will be able to contact Candidates via Vested’s site. As a Client, You acknowledge your requirement to promptly notify Vested if a candidate found on Vested has accepted an offer. Notification includes a copy of the approved offer, start date, and compensation. If a Candidate is identified through the use of our Service and accepts an Employment Offer (full-time, contract, or other) within (and including) 36 months of first viewing that Candidate on Vested, the Client will be charged a Success Fee. This Success Fee is equal to 20% of the Candidate’s first year base salary. Client agrees to pay the fees for the Services in accordance with the payment terms stated in the ordering document.
				</p>
            <p>
                In order to avoid paying the Success Fee, the Client must establish and show clearly that the Client had an Active Process with the Candidate before using our Site and Service (e.g., the Candidate had already begun the interview process with the Client and such process had not been terminated). “Active Process” shall mean continuous direct, back & forth communication, in an active recruiting or hiring context where a decision to put a candidate on hold or reject has not been made, within the three (3) months prior to using the Site or Service for a Candidate that exists in Client’s applicant tracking system or that was submitted by a recruiting agency.
				</p>
            <p>
                If a Client hires a Candidate and terminates the Candidate’s Employment based on unsatisfactory performance within ninety (90) days of the Start Date, or a Candidate voluntarily terminates his or her Employment within ninety (90) days of the Start Date, upon written receipt and confirmation of such information, Vested will fully refund the Success Fee if the Success Fee was paid by Client prior to the Termination Event.
				</p>
            <p>
                Vested may suspend Client’s access if Client is in breach of the Agreement or late on payment. This suspension may continue for as long as reasonably necessary for Vested and the Client to remedy the situation.
				</p>
            <p>
                If a Client circumvents our Site and/or Service after discovering a Candidate through our Site or Service and subsequently hires that Candidate within (and including) 365 days from when the Client first viewed the Candidate on the Site, the Client will pay a Success Fee equal to 25% of the 1st year base salary or prorated contractor compensation of the Candidate. Additionally, Vested reserves the right to terminate the Client’s account. YOUR OBLIGATION TO PAY ANY SUCCESS FEE SURVIVES ANY TERMINATION OF THE ACCOUNT OR THIS AGREEMENT.
				</p>

            <h2>General</h2>
            <p>The following section includes ancillary yet important terms of service</p>
            <h3>Licensing to Vested</h3>
            <p>
                The design of the Service along with Vested created text, scripts, graphics, interactive features and the trademarks, service marks and logos contained therein are owned by or licensed to Vested, subject to copyright and other intellectual property rights under United States and foreign laws and international conventions. Vested reserves all rights in and to the Service and the Site. You agree to not engage in the use, copying, or distributing any content contained within the Site or through the Service unless we have given You express written permission to do so.
				</p>
            <p>
                You hereby grant to Vested the ability to (1) display, publicly perform, distribute, store, broadcast, transmit and reproduce your logo(s), service marks, trademarks and trade names through the Site, the Service and/or any other medium currently invented or invented in the future; and (2) display, publicly perform, distribute, store, broadcast, transmit, reproduce, modify, prepare derivative works and otherwise use and reuse all or any part of the Content provided / uploaded. By submitting any Content to us, You hereby represent and warrant that You own all rights to the Content or, alternatively, that You have the right to give us the license described above
				</p>
            <p>
                Clients and Candidates are not obligated to provide Vested with any suggestions, enhancement requests, or other feedback about the Services or related technology. However, if Client or Candidate does provide any feedback to Vested, Vested may use and modify it without any restriction or payment.
				</p>
            <h3>Force Majeure</h3>
            <p>
                You agree that We are not responsible to You for anything that we may otherwise be responsible for, if it is the result of events beyond our control, including, but not limited to, acts of God, war, insurrection, riots, terrorism, crime, labor shortages (including lawful and unlawful strikes), embargoes, postal disruption, communication disruption, failure or shortage of infrastructure, shortage of materials, or any other event beyond our control.
				</p>
        </section>
    );
}

export default Terms