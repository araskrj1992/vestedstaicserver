import React from "react";

const Privacy = () => {
    return (
        <section className="terms">
            <h1>Privacy Policy </h1>
            <p>
                Vested Technology LLC (“Vested”) recognizes that your privacy is very important and takes it seriously. This Privacy Policy describes Vested’s policies and procedures on the collection, use, disclosure, and sharing of your information (“Personal Data” as defined below) when you use the Vested Service.
				</p>
            <h2>1. Questions; Reporting Violations</h2>
            <p>
                If you have questions, concerns, or complaints about this Policy, or if you want to report any security
                violations please contact us at the following:
					<br />
                info@getvested.io
				</p>
            <h2>2. User Consent</h2>
            <p>
                By submitting Personal Data through our Site or Service (as defined under the Terms of Service), you agree to the terms of this Privacy Policy and you expressly consent to the collection, use and disclosure of your Personal Data in accordance with this Privacy Policy.
				</p>
            <h2>3. Users From Outside Of The United States</h2>
            <p>
                If you are a non-U.S. user of the Site, you acknowledge and agree that your Personal Data may be processed for the purposes identified in this Policy. In addition, your Personal Data may be processed in the country in which it was collected and in other countries, including the United States, where laws regarding processing of Personal Data may be less stringent than the laws in your country. By providing your data, you consent to such transfer.
				</p>
            <h2>4. Types Of Data We Collect</h2>
            <p>
                “Personal Data” means data that allows someone to identify or contact you, including, for example, your name, address, telephone number, e-mail address, social security number or taxpayer ID, as well as any other non-public information about you that is associated with or linked to any of the foregoing data. “Anonymous Data” means data that is not associated with or linked to your Personal Data; Anonymous Data does not, by itself, permit the identification of individual persons. We collect Personal Data and Anonymous Data, as described below.
				</p>
            <h3>User-Provided Information:</h3>
            <p>
                We may collect Personal Data from you, such as your first and last name, location, phone number, gender, e-mail and mailing addresses, professional title, company name, employment history, education history, personal summary, and password when you create an account to log in to our network (“Account”). We retain information on your behalf, such as files and messages that you store using your Account. If you provide us feedback or contact us via e-mail, we will collect your name and e-mail address, as well as any other content included in the e-mail, in order to send you a reply. When you post content (text, images, photographs, messages, comments or any other kind of content that is not your e-mail address) on our Site, the information contained in your posting will be stored in our servers and other users will be able to see it, along with your profile photo and any other information that you choose to make public on your public profile page (“Profile”). The information that you provide in your Profile will be visible to others, including anonymous visitors to the Site.
				</p>
            <p>
                We also collect other types of Personal Data that you provide to us voluntarily, such as your operating system and version, product registration number, and other requested information if you contact us via e-mail regarding support for the Service. We may also collect Personal Data at other points in our Site that state that Personal Data is being collected.
				</p>
            <p>
                If you use your LinkedIn, Facebook, Twitter, or other social networking site (“SNS”) account information to sign in to Vested, we will collect and store your SNS user ID. If you connect your Vested account with your SNS account (such as to enable posting from Vested), we ask for your permission to collect certain information from your SNS account (such as information from your SNS profile).
				</p>
            <p>
                If you choose to use our invitation service to invite a friend to the Service, we will ask you for that
                person’s email address and automatically send an email invitation. You may also use our contact
                importer tool to help you upload your friends’ email addresses so you can find friends who are already
                on Vested or invite friends to the Service. Vested stores this information to send invitations, to register
                your friend if your invitation is accepted, to track the success of our invitation service, and to identify
                your friends on Vested. By providing your friends’ email addresses to Vested, you warrant that you have
                their consent to do so and for Vested to use this information as described above.
				</p>
            <h3>Information Collected Automatically</h3>
            <p>
                When you use the Service, we use persistent and session cookies (for information on cookies, please see
                below) and other tracking technologies such as log files, clear GIFs, and Flash technologies to: (a) store
                your username and password; (b) analyze the usage of the Service; (c) customize the Service to your
                preferences; and (d) control the advertising displayed by the Service. We also may include clear GIFs in
                HTML-based emails sent to our users to determine whether the message has been opened. As we adopt
                additional technology, we may also gather additional information through other methods.
				</p>
            <p>
                We use these automated technologies to collect and analyze certain types of information, including: (a)
                information related to the devices or browsers you use to access or interact with the Service, such as: IP
                addresses, geolocation information, unique device identifiers and other information about your mobile
                phone or other mobile device(s), browser types, browser language, and unique numbers or codes in
                cookies; and (b) information related to the ways in which you interact with the Service, such as: referring
                and exit pages and URLs, platform type, the number of clicks, domain names, landing pages, pages
                viewed and the order of those pages, the amount of time spent on particular pages, the date and time
                you used the Service, and other similar information. We may also capture other data, such as search
                criteria and results.
				</p>
            <p>
                We may collect different types of information about your location, including general information (e.g., IP
                address, zip code) and more specific information (e.g., GPS-based functionality on mobile devices used
                to access the Service), and may use that information to customize the Service with location-based
                information and features. If you access the Service through a mobile device and you do not want your
                device to provide us with location-tracking information, you can disable the GPS or other
                location-tracking functions on your device, provided your device allows you to do this.
				</p>
            <h3>Cookies</h3>
            <p>
                A cookie is a small text file stored by a website in a user’s web browser (e.g. Internet Explorer, Safari,
                Firefox or Chrome) that helps us in many ways to make your visit to our website more enjoyable and
                meaningful to you. Among other things, cookies avoid you having to log in every time you come back to
                our website. They also allow us to tailor a website or advertisement to better match your interests and
                preferences.
				</p>
            <p>
                A session cookie is stored only in your computer’s working memory (RAM) and only lasts for your
                browsing session. When you close all your browser’s windows, or when you shut down your computer,
                the session cookie disappears forever.
				</p>
            <p>
                A persistent cookie remains on your computer after you close your browser so that it can be used by
                your browser on subsequent visits to the Service. Persistent cookies stay on your computer until either
                they expire or are overwritten with newer cookies, or you manually remove them. Most browsers can
                be configured not to accept cookies, however, this may prevent you from having access to some site
                functions or features.
				</p>
            <p>
                While specific names of the cookies and similar technologies that we use may change from time to time
                as we improve and update our services, they generally fall into the below categories of use:
				</p>
            <p>
                Authentication and security cookies. We use these cookies to enable you to remain logged into Vested,
                and verify that it is you as you use Vested. This helps keep your account safe and secure from
                unauthorized use, and helps combat spam and other abuse which violates our policies.
                Analytics and research cookies. We use these cookies to better understand how people use Vested. For
                example, how often particular features are used, or which content leads towards user activity.
				</p>
            <p>
                Product features and setting cookies. We use these cookies to enable the functionality of some features
                within the Vested product, in particular to personalize the experience towards you. We also use these
                cookies to store certain of your preferences and settings.
				</p>
            <p>
                Advertising cookies. We use these cookies to help deliver relevant ads to you and measure the
                performance of ads.
				</p>
            <p>
                Flash Cookies. We may, in certain situations, use Adobe Flash Player to deliver special content, such as
                video clips or animation. To improve your user experience, Local Shared Objects (commonly known as
                "Flash cookies") are employed to provide functions such as remembering your settings and preferences.
                Flash cookies are stored on your device, but they are managed through an interface different from the
                one provided by your web browser. This means it is not possible to manage Flash cookies at the browser
                level, in the same way you would manage cookies. Instead, you can access your Flash management tools
                from Adobe's website directly. The Adobe website provides comprehensive information on how to
                delete or disable Flash cookies see adobe.com/security/flashplayer for further information. Please be
                aware that if you disable or reject Flash cookies, you may not be able to access certain features, such as
                video content or services that require you to sign in.
				</p>
            <p>
                For additional information on how to block cookies, please refer to the privacy or security settings of
                your browser.
				</p>
            <h3>Third Party Analytics and Advertising</h3>
            <p>
                Vested may allow third parties to help us collect and analyze information about your use of the Service,
                generate aggregate site usage statistics and provide content sharing services to support the Service.
                These third parties may also use cookies and similar technologies to collect similar information about
                your use of the Service. Vested does not control the third parties’ use of such technologies and their use
                is governed by those parties’ privacy policies.
				</p>
            <p>
                Vested may also allow third-party ad servers or ad networks to serve advertisements on the Service and
                on third-party services. These third-party ad servers or ad networks use technology to send, directly to
                your browser, the advertisements and links that appear on Vested. They automatically receive your IP
                address when this happens. They may also use other technologies (such as cookies, JavaScript, or web
                beacons) to measure the effectiveness of their advertisements and to personalize the advertising
                content. Vested does not provide any information that can identify you personally to these third-party
                ad servers or ad networks without your consent. However, please note that if an advertiser asks Vested
                to show an advertisement to a certain audience and you respond to that advertisement, the advertiser
                or ad server may conclude that you fit the description of the audience they are trying to reach.
                Vested itself does not respond to “do not track” signals, and we do not control whether third parties do.
                For more information about third-party ad networks that use cookies and the other technologies
                discussed above, and to opt-out of their tailoring of ads on third-party sites based on this information.
				</p>
            <h2>5. Use Of Your Personal Data</h2>
            <h3>A. General Use</h3>
            <p>
                In general, Personal Data you submit to us is used either to respond to requests that you make, or to aid
                us in serving you better. We use your Personal Data in the following ways:
				</p>
            <ul>
                <li>facilitate the creation of and completion of your profile on our Site;</li>
                <li>identify you as a user in our system;</li>
                <li>provide improved administration of our Site and Service;</li>
                <li>provide the Service;</li>
                <li>improve the quality of experience when you interact with our Site and Service;</li>
                <li>send you a welcome e-mail to verify ownership of the e-mail address provided when your Account was created;</li>
                <li>
                    send you administrative e-mail notifications, such as information about pending job offers,security or support and maintenance advisories;
					</li>
                <li>respond to your inquiries related to employment opportunities or other requests;</li>
                <li>make telephone calls to you, from time to time, as a part of secondary fraud protection or to solicit your feedback;</li>
                <li>send you calendar invitations; and</li>
                <li>
                    send newsletters, surveys, offers, and other promotional materials related to our Service and for other marketing purposes of Vested.
					</li>
            </ul>
            <h3>B. User Testimonials And Feedback</h3>
            <p>
                We often receive testimonials and comments from users who have had positive experiences with our
                Service. We occasionally publish such content. When we publish this content, we may identify our users
                by their first and last name and may also indicate their home city. We obtain the user’s consent prior to
                posting his or her name along with the testimonial. We may post user feedback on the Site from time to
                time. We will share your feedback with your first name and last initial only. If we choose to post your
                first and last name along with your feedback, we will obtain your consent prior to posting you name with
                your feedback. If you make any comments on a blog or forum associated with your Site, you should be
                aware that any Personal Data you submit there can be read, collected, or used by other users of these
                forums, and could be used to send you unsolicited messages. We are not responsible for the personally
                identifiable information you choose to submit in these blogs and forums.
				</p>
            <h2>6. Disclosure Of Your Personal Data</h2>
            <p>We disclose your Personal Data as described below and as described elsewhere in this Privacy Policy.</p>
            <h3>A. Third Parties Designated By You</h3>
            <p>
                When you use the Service, the Personal Data you provide will be shared with the third parties that you
                designate to receive such information, including other websites, your friends, relatives and business
                associates. Depending on the type of access you grant to such third parties, they may also be permitted
                to edit the information you have provided to us and to designate others to access and edit such
                information. You may change your settings at any time as to who has access to your information by
                going to your account settings and changing your publishing options.
				</p>
            <h3>B. Third Party Service Providers</h3>
            <p>
                We may share your Personal Data with third party service providers to: provide you with the Service that
                we offer you through our Site; to conduct quality assurance testing; to facilitate creation of accounts; to
                provide technical support; and/or to provide other services to Vested. These third party service
                providers are required not to use your Personal Data other than to provide the services requested by
                Vested.
				</p>
            <h3>C. Affiliates</h3>
            <p>
                We may share some or all of your Personal Data with our parent company, subsidiaries, joint ventures,
                or other companies under a common control (“Affiliates”), in which case we will require our Affiliates to
                honor this Privacy Policy.
				</p>
            <h3>D. Public Profile</h3>
            <p>
                Certain portions of the information you provide to us may also be displayed in your Profile. As an
                essential element of the Service, most of the Personal Data you explicitly provide to us when you
                register or update your Profile is displayed on your Profile. In order for your Profile to be made public,
                you must go to your profile settings and then to profile visibility. By default, your Profile is not for public
                viewing. Your photos, posts, friends, and other content you post to the Site are also meant for public
                consumption. We may display this content on the Site and further distribute it to a wider audience
                through third party sites and services. Once displayed on publicly viewable web pages, that information
                can be collected and used by others. We cannot control who reads your postings or what other users
                may do with the information that you voluntarily post, so it is very important that you do not put
                Personal Data in your posts. Once you have posted information publicly, while you will still be able to
                edit and delete it on the Site, you will not be able to edit or delete such information cached, collected,
                and stored elsewhere by others (e.g., search engines).
				</p>
            <h3>E. Other Disclosures</h3>
            <p>
                Regardless of any choices you make regarding your Personal Data (as described below), Vested may
                disclose Personal Data if it believes in good faith that such disclosure is necessary (a) in connection with
                any legal investigation; (b) to comply with relevant laws or to respond to subpoenas or warrants served
                on Vested; (c) to protect or defend the rights or property of Vested or users of the Site or Service;
                and/or (d) to investigate or assist in preventing any violation or potential violation of the law, this
                Privacy Policy, or our Terms of Use.
				</p>
            <h2>7. Third Party Websites</h2>
            <p>
                Our Site may contain links to third party websites. When you click on a link to any other website or
                location, you will leave our Site and go to another site, and another entity may collect Personal Data or
                Anonymous Data from you. We have no control over, do not review, and cannot be responsible for,
                these outside websites or their content. Please be aware that the terms of this Privacy Policy do not
                apply to these outside websites or content, or to any collection of your Personal Data after you click on
                links to such outside websites.
				</p>
            <h2>8. Your Choices Regarding Information</h2>
            <p>You have several choices regarding the use of information on our Service:</p>
            <h3>A. Email Communications</h3>
            <p>
                We will periodically send free newsletters and e-mails that directly promote the use of our Site or
                Service. When you receive newsletters or promotional communications from us, you may indicate a
                preference to stop receiving further communications from us and you will have the opportunity to
                “opt-out” by following the unsubscribe instructions provided in the e-mail you receive. Despite your
                indicated e-mail preferences, we may send you service related communications, including notices of any
                updates to our Terms of Use or Privacy Policy.
				</p>
            <h3>B. Changing Or Deleting Your Personal Data</h3>
            <p>
                You may change any of your Personal Data in your Account by editing your profile within your Account.
                You may request deletion of your Personal Data by us, and we will use commercially reasonable efforts
                to honor your request, but please note that we may be required to keep such information and not
                delete it (or to keep this information for a certain time, in which case we will comply with your deletion
                request only after we have fulfilled such requirements). When we delete any information, it will be
                deleted from the active database, but may remain in our archives. We may also retain your information
                for fraud or similar purposes.
				</p>
            <h2>9. Security Of Your Personal Data</h2>
            <p>
                Vested is committed to protecting the security of your Personal Data. We use a variety of
                industry-standard security technologies and procedures to help protect your Personal Data from
                unauthorized access, use, or disclosure. We also require you to enter a password to access your Account
                information. Please do not disclose your Account password to unauthorized people. No method of
                transmission over the Internet, or method of electronic storage, is 100% secure, however. Therefore,
                while Vested uses reasonable efforts to protect your Personal Data, Vested cannot guarantee its
                absolute security.
				</p>
            <h2>11. Changes To This Privacy Policy</h2>
            <p>
                This Privacy Policy may be updated from time to time for any reason. We will notify you of any changes
                to our Privacy Policy by posting the new Privacy Policy and we will change the “Last Updated” date
                above. You should consult this Privacy Policy regularly for any changes. Continued use of our Site or
                Service, following posting of such changes, shall indicate your acknowledgement of such changes and
                agreement to be bound by the terms of such changes.
				</p>
        </section>
    );
}


export default Privacy;