import React from "react";
import { NavLink } from "react-router-dom"

const ProfileSummary = (props) => {
    return (
        <div className="profile-summary">
            <div className="summary-wrapper">
                <img src="/images/profile/jorge.png" alt="Jorge Dueñas"></img>
                <div style={{ paddingLeft: '10px' }}>
                    <h3>Jorge Dueñas, CPA <img src="/images/profile/linked-in.png" alt="graduation" /></h3>
                    <h4 className="recent-post">Senior Associate - FDD Mergers & Acquisitions</h4>
                    <p className="last-education"><img src="/images/profile/graduate.png" alt="graduation" />
                        <span>B.S. in Finance, Brown University</span><span className="year">2010</span></p>
                </div>
            </div>
            <div className="profile-nav">
                <NavLink to="/" activeClassName="activelink">Higlights</NavLink>
                <NavLink to="/" activeClassName="activelink">Resume</NavLink>
                <NavLink to="/" activeClassName="activelink">Candidate Notes</NavLink>
            </div>
        </div>
    );
}


export default ProfileSummary;
