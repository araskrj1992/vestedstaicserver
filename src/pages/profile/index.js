import React, { Fragment } from "react";
import { Switch, Route, Redirect, withRouter } from "react-router-dom"
import Sidebar from './sidebar'
import ProfileSummary from "./profileSummary"
import Highlights from "./highlights"

class Profile extends React.Component {

    render() {
        return (
            <Fragment>
                <section className="profile">
                    <Sidebar />
                    <div>
                        <div className="profile-info-wrapper">
                            <ProfileSummary />
                            <Switch>
                                <Route path={`${this.props.match.path}/:id/highlights`} render={() => <Highlights {...this.props} />} exact />
                                <Redirect to="/" />
                            </Switch>
                        </div>
                    </div>
                </section>
            </Fragment>
        );
    }
};



export default withRouter(Profile);
