import React from "react";

const Sidebar = () => {
    return (
        <div className="sidebar">
            <h2 className="job-title">Partner, Deployment Strategy Lead</h2>
            <h6 className="side-heading">New Candidates <span>3</span></h6>
            <div className="small-info-wrapper active">
                <h2>Jorge Dueñas, CPA</h2>
                <p>Miami Area</p>
            </div>
            <div className="small-info-wrapper">
                <h2>Jorge Dueñas, CPA</h2>
                <p>Miami Area</p>
            </div>
            <div className="small-info-wrapper">
                <h2>Jorge Dueñas, CPA</h2>
                <p>Miami Area</p>
            </div>
            <h6 className="side-heading mar34">Screened Candidates <span>3</span></h6>
            <div className="small-info-wrapper">
                <h2>Jorge Dueñas, CPA</h2>
                <p>Miami Area</p>
            </div>
            <div className="small-info-wrapper">
                <h2>Jorge Dueñas, CPA</h2>
                <p>Miami Area</p>
            </div>
        </div>
    );
}


export default Sidebar;
