import React from "react";
import { Row, Col } from "react-bootstrap"


const Highlights = (props) => {
    return (
        <div className="highlights-summary">
            <Row>
                <Col xs={12} sm={12} md={12} className="higlight-summary-wrapper">
                    <h4>Qualification summary</h4>
                    <Row className="qualification">
                        <Col xs={12} sm={6} md={6}>
                            <h5>Diligence</h5>
                            <p>Current role is performing financial due diligence on mergers and acquisitions for corporate and private equity clients. Performs high level analysis and runs meetings. He doesn’t put together the actual model, but evaluates client models.</p>
                        </Col>
                        <Col xs={12} sm={6} md={6}>
                            <h5>Sourcing</h5>
                            <p>Active in business development and recruiting efforts.</p>
                        </Col>
                        <Col xs={12} sm={6} md={6}>
                            <h5>Advisory</h5>
                            <p>Advise clients on GAAP accounting matters and treatment of certain inputs within the SPA (e.g. cash, indebtedness, and target working capital)</p>
                        </Col>
                        <Col xs={12} sm={6} md={6}>
                            <h5>Reporting</h5>
                            <p>Prepare ad hoc financial analyses as requested by client, such as price / volume and sensitivity analyses</p>
                        </Col>
                    </Row>
                </Col>
            </Row>
            <Row>
                <Col xs={12} sm={6} md={6} className="higlight-summary-wrapper">
                    <h4>Recent Experience Summary</h4>
                    <Row className="qualification">
                        <Col xs={12} sm={12} md={12}>
                            <h5>PwC - Assurance Associate (1/14-7/16) & Senior Associate - FDD Mergers & Acquisitions (7/16-Present)</h5>
                            <p>Interned there, and started as Assurance Associate which is audit. Worked with Oracle and People Soft ERP on the reporting side. After 2 ½ years he transferred to M&A under the advisory umbrella working on financial due diligence. When a client wants to acquire another company or is selling off, he coordinates the entire diligence process. This includes high level analysis; running the majority of the management meeting because he’s close to the data and process; putting together a report/deliverable for the client; and helping with pre and post close work. His business development work is going to lunch with prospects and recruiting for the firm. They don’t put together models for clients, but rather help them with the inputs going into the model. He knows not having modeling experience hurts him, but he feels he can learn that, and his value is that he brings the front facing soft skills – his role now includes talking to CEOs, CFOs, etc. </p>
                        </Col>
                    </Row>
                </Col>
                <Col xs={12} sm={6} md={6} className="higlight-summary-wrapper">
                    <h4>Additional Information</h4>
                    <Row className="qualification">
                        <Col xs={12} sm={12} md={12}>
                            <h5>Salary Expectations</h5>
                            <p>Currently $90K + $12K bonus but next year he’ll be a manager so he’ll be $102-$105K + 20% bonus; is focused on over $100K base if he leaves. Current Location:  Miami/Fort Lauderdale Area</p>
                        </Col>
                    </Row>
                    <Row className="qualification">
                        <Col xs={12} sm={12} md={12}>
                            <h5>Ideal next step in career, and current job search status</h5>
                            <p>Started in big 4 public accounting right out of school, and it’s been almost 5 years. He knew he didn’t want to stay forever. The job market is good right now so he’s exploring what’s out there. Interested in corporate development and strategy roles – that’s what he’s been applying to. Long-term goal is maybe get into management.</p>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </div>
    );
}


export default Highlights;
