import React, { Component } from "react";
import { Route, Switch, Redirect, withRouter } from "react-router-dom";
import MokaBrand from "../branding/moka"
import VPDeployment from "../branding/moka/jobDescription/VPDdeployment";
import SeniorSoftwareEngineer from "../branding/moka/jobDescription/SeniorSoftwareEngineer";
import DataEngineer from "../branding/moka/jobDescription/DataEngineer";
import EndeavorJobDescription from "../branding/endeavor/jobDescription";

class VestedRoutes extends Component {
    componentDidUpdate(prevProps) {
        if (this.props.location.pathname !== prevProps.location.pathname) {
            window.scrollTo(0, 0)
        }
    }


    render() {
        return (
            <Switch>
                <Route path="/moka" component={MokaBrand} exact />
                <Route path="/moka/jobs/vp-deployment" render={() => <VPDeployment {...this.props} />} />
                <Route path="/moka/jobs/sw-eng" render={() => <SeniorSoftwareEngineer {...this.props} />} />
                <Route path="/moka/jobs/data-eng" render={() => <DataEngineer {...this.props} />} />
                <Route path="/endeavor/jobs/corporate-development-lead" render={() => <EndeavorJobDescription {...this.props} />} />
                <Redirect to="/" />
            </Switch>
        )
    }
};

export default withRouter(VestedRoutes);
