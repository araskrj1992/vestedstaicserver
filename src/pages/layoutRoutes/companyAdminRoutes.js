import React, { Component } from "react";
import { Route, Switch, Redirect, withRouter } from "react-router-dom";
import CompanyAdmin from '../companyAdmin'

class CompanyAdminRoutes extends Component {
    componentDidUpdate(prevProps) {
        if (this.props.location.pathname !== prevProps.location.pathname) {
            window.scrollTo(0, 0)
        }
    }
    render() {
        return (
            <Switch>
                <Route path="/company-admin/:id/job/:id" component={CompanyAdmin} exact />
                <Redirect to="/" />
            </Switch>
        )
    }
};

export default withRouter(CompanyAdminRoutes);
