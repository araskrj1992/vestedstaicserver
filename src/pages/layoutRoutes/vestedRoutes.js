import React, { Component } from "react";
import { Route, Switch, Redirect, withRouter } from "react-router-dom";
import Home from "../home";
import JobSeekers from "../job-seekers";
import Careers from "../careers";
import Services from "../services";
import Terms from "../documents/terms";
import Privacy from "../documents/privacy";

class VestedRoutes extends Component {


    componentDidUpdate(prevProps) {
        if (this.props.location.pathname !== prevProps.location.pathname) {
            window.scrollTo(0, 0)

        }
    }
    render() {
        return (
            <Switch>
                <Route exact path="/" render={() => <Home {...this.props} />} />
                <Route path="/job-seekers" component={JobSeekers} />
                <Route path="/joinus" component={Careers} />
                <Route path="/services" render={() => <Services {...this.props} />} />
                <Route path="/terms-of-service" component={Terms} />
                <Route path="/privacy-policy" component={Privacy} />
                <Redirect to="/" />
            </Switch>
        )
    }
};

export default withRouter(VestedRoutes);
