import React, { Fragment } from "react";
import Header from "./header"
import { Row, Col } from "react-bootstrap"
import { CopyToClipboard } from "react-copy-to-clipboard"
import { makeRequest } from '../../helper'
import FormSuccess from '../common/successModal'

import {
    FacebookShareButton,
    GooglePlusShareButton,
    LinkedinShareButton,
    TwitterShareButton
} from 'react-share';

const companyDetails = [
    {
        companyName: "Tesla Motor's",
        package: "Sourcing + Screening",
        logoUrl: "/images/companyAdmin/companyLogos/teslaMotors.png"
    }
]

const candidates = [
    {
        name: "Tyler Durden",
        candidateImg: "/images/companyAdmin/candidates/candidate.png",
        social: {
            linkedin: "https://www.linkedin.com/in/ankit-gupta-b95815a9/",
            cv: "https://www.linkedin.com/in/ankit-gupta-b95815a9/"
        },
        presentJob: {
            jobTitle: "Director of Finance",
            company: "Handy"
        },
        highestEducation: {
            branch: "B.S in Finance",
            university: "Brown University"
        }
    },
    {
        name: "Tyler Durden",
        candidateImg: "/images/companyAdmin/candidates/candidate.png",
        social: {
            linkedin: "https://www.linkedin.com/in/ankit-gupta-b95815a9/",
            cv: "https://www.linkedin.com/in/ankit-gupta-b95815a9/"
        },
        presentJob: {
            jobTitle: "Director of Finance",
            company: "Handy"
        },
        highestEducation: {
            branch: "B.S in Finance",
            university: "Brown University"
        }
    },
    {
        name: "Tyler Durden",
        candidateImg: "/images/companyAdmin/candidates/candidate.png",
        social: {
            linkedin: "https://www.linkedin.com/in/ankit-gupta-b95815a9/",
            cv: "https://www.linkedin.com/in/ankit-gupta-b95815a9/"
        },
        presentJob: {
            jobTitle: "Director of Finance",
            company: "Handy"
        },
        highestEducation: {
            branch: "B.S in Finance",
            university: "Brown University"
        }
    },
    {
        name: "Tyler Durden",
        candidateImg: "/images/companyAdmin/candidates/candidate.png",
        social: {
            linkedin: "https://www.linkedin.com/in/ankit-gupta-b95815a9/",
            cv: "https://www.linkedin.com/in/ankit-gupta-b95815a9/"
        },
        presentJob: {
            jobTitle: "Director of Finance",
            company: "Handy"
        },
        highestEducation: {
            branch: "B.S in Finance",
            university: "Brown University"
        }
    },
    {
        name: "Tyler Durden",
        candidateImg: "/images/companyAdmin/candidates/candidate.png",
        social: {
            linkedin: "https://www.linkedin.com/in/ankit-gupta-b95815a9/",
            cv: "https://www.linkedin.com/in/ankit-gupta-b95815a9/"
        },
        presentJob: {
            jobTitle: "Director of Finance",
            company: "Handy"
        },
        highestEducation: {
            branch: "B.S in Finance",
            university: "Brown University"
        }
    },
    {
        name: "Tyler Durden",
        candidateImg: "/images/companyAdmin/candidates/candidate.png",
        social: {
            linkedin: "https://www.linkedin.com/in/ankit-gupta-b95815a9/",
            cv: "https://www.linkedin.com/in/ankit-gupta-b95815a9/"
        },
        presentJob: {
            jobTitle: "Director of Finance",
            company: "Handy"
        },
        highestEducation: {
            branch: "B.S in Finance",
            university: "Brown University"
        }
    }
]

export default class JobAdmin extends React.Component {

    state = {
        tabKey: 1,
        copied: false,
        upgrading: false,
        formSendSuccess: false
    }

    chageTab(key) {
        this.setState({ tabKey: key });
    }

    onCopy = () => {
        this.setState({ copied: true });
        setTimeout(() => {
            this.setState({ copied: false })
        }, 1000, this)
    };

    upgrade = async () => {
        let data = JSON.stringify({
            msg: "Tesla motors want to upgarde their package to assesed"
        })
        try {
            this.setState({ upgrading: true })
            let fetchResult = await makeRequest('POST', '/upgradePackage', data)
            fetchResult = JSON.parse(fetchResult)
            if (fetchResult.success) {
                this.setState({ upgrading: false, formSendSuccess: true })
            }
        } catch (err) {
            // this.setState({ isLoading: false, serverError: "There is some server error" })
        }
    }

    render() {
        return (
            <section className="company-admin">
                <Header companyDetails={companyDetails[0]} />
                {/* <div className="tab-wrapper">
                    <div className="tab-content-wrapper">
                    </div>
                </div> */}
                <div className="whole-wrapper">
                    <div className="company-content">
                        <CompanyJobs copyToClipboard={() => this.copyToClipboard()} onCopy={() => this.onCopy()} copied={this.state.copied} upgrading={this.state.upgrading} upgrade={() => this.upgrade()} />
                    </div>
                </div>
                <FormSuccess
                    successPopup={() => this.setState({ formSendSuccess: !this.state.formSendSuccess })}
                    formSendSuccess={this.state.formSendSuccess}
                />
            </section>
        );
    };
};

const CompanyJobs = (props) => {

    let shareUrl = (window.location.origin + window.location.pathname).trim();
    return (
        <Fragment>
            <div className="job-content">
                <div className="job-details-wrapper">
                    <h4>Controller, Accounting <img src="/images/companyAdmin/down.svg" alt="down" /></h4>
                    <div>
                        <p>Project Duration <span className="date">15th September, 2018 - </span><span className="status">ongoing</span></p>
                    </div>
                </div>
                <Row>
                    <Col xs={12} sm={9} md={9} >
                        <Row className="view-hours">
                            <Col md={4} className="">
                                <div className="view-hour-content">
                                    <h6>30 <span>hrs</span></h6>
                                    <p>Spent On Role</p>
                                </div>
                            </Col>
                            <Col md={4} className="">
                                <div className="view-hour-content">
                                    <h6>121</h6>
                                    <p>Active Candidates</p>
                                </div>
                            </Col>
                            <Col md={4} className="">
                                <div className="view-hour-content">
                                    <h6>431</h6>
                                    <p>Branded Job Views</p>
                                </div>
                            </Col>
                        </Row>
                        <Row className="job-candidates-wrapper">
                            <Col xs={12} sm={12} md={12}>
                                <h6>Candidates</h6>
                                <div className="table-wrapper">
                                    <table>
                                        <thead>
                                            <tr>
                                                <th>Candidate Info</th>
                                                <th>Recent job title</th>
                                                <th>Company</th>
                                                <th>Education</th>
                                                <th className="date-heading">Delivery Date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                candidates.map((item, i) => {
                                                    return (
                                                        <tr key={"candidate" + i}>
                                                            <td>
                                                                <p className="candidate-info">
                                                                    <img className="mr5" src={item.candidateImg} alt={item.name} />
                                                                    <span className="mr5">{item.name}</span>
                                                                    <a href={item.social.linkedin}
                                                                        target="_blank"
                                                                        rel="noopener noreferrer"><img className="mr5" src="/images/companyAdmin/linkedin.svg" alt="candidate" />
                                                                    </a>
                                                                    <a href={item.social.cv}
                                                                        target="_blank"
                                                                        rel="noopener noreferrer">
                                                                        <img src="/images/companyAdmin/cv.svg" alt="candidate" />
                                                                    </a>
                                                                </p>
                                                            </td>
                                                            <td>{item.presentJob.jobTitle}</td>
                                                            <td>{item.presentJob.company}</td>
                                                            <td><p className="education">{item.highestEducation.branch}</p><p className="university">{item.highestEducation.university}</p></td>
                                                            <td className="date">15 Sep 2018</td>
                                                        </tr>
                                                    )
                                                })
                                            }
                                        </tbody>
                                    </table>
                                </div>
                            </Col>
                        </Row>
                        <Row className="share-social-wrapper">
                            <Col xs={12} sm={12} md={12}>
                                <h5>Want to widen your funnel? Share the job on social media</h5>
                                <div className="social-icon-wrapper">
                                    <FacebookShareButton url={shareUrl} className="share-button">
                                        <img src="/images/companyAdmin/social/facebook.svg" alt="facebook" />
                                    </FacebookShareButton>
                                    <LinkedinShareButton
                                        url={shareUrl}
                                        windowWidth={750}
                                        windowHeight={600}
                                        className="share-button">
                                        <img src="/images/companyAdmin/social/linkedin.svg" alt="linkedin" />
                                    </LinkedinShareButton>
                                    <GooglePlusShareButton
                                        url={shareUrl}
                                        className="share-button">
                                        <img src="/images/companyAdmin/social/google.svg" alt="google plus" />
                                    </GooglePlusShareButton>
                                    <TwitterShareButton
                                        url={shareUrl}
                                        className="share-button">
                                        <img src="/images/companyAdmin/social/twitter.svg" alt="twitter" />
                                    </TwitterShareButton>
                                    <div className="copy-to-clipboard">
                                        <img src="/images/companyAdmin/copy.svg" alt="copy" />
                                        <p className="link" id="copyInput">{shareUrl}</p>
                                        <CopyToClipboard onCopy={() => props.onCopy()} text={shareUrl}>
                                            <span className="copy">COPY
                                    {props.copied && <span className="copied">Copied</span>}</span>

                                        </CopyToClipboard>

                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </Col>
                    <Col xs={12} sm={3} md={3} >
                        <div className="funnel-wrapper">
                            <h5>Funnel</h5>
                            <div className="funnel-category">
                                <div className="category-progress"></div>
                                <div className="category-wrapper">
                                    <p><span>Contacted</span><span className="category-percentage">100%</span></p>
                                    <p className="category-candidate">65</p>
                                </div>
                            </div>
                            <div className="funnel-category interested">
                                <div className="category-progress" style={{ width: "85%" }}></div>
                                <div className="category-wrapper">
                                    <p><span>Interested</span><span className="category-percentage">85%</span></p>
                                    <p className="category-candidate">65</p>
                                </div>
                            </div>
                            <div className="funnel-category resumes-screened">
                                <div className="category-progress" style={{ width: "60%" }}></div>
                                <div className="category-wrapper">
                                    <p><span>Resumes Screened</span><span className="category-percentage">60%</span></p>
                                    <p className="category-candidate">65</p>
                                </div>
                            </div>
                            <div className="funnel-category candidate-screened" >
                                <div className="category-progress" style={{ width: "45%" }}></div>
                                <div className="category-wrapper">
                                    <p><span>Candidates Screened</span><span className="category-percentage">45%</span></p>
                                    <p className="category-candidate">65</p>
                                </div>
                            </div>
                            <div className="funnel-category assessed" >
                                <div className="category-progress" style={{ width: "35%" }}></div>
                                <div className="category-wrapper">
                                    <p><span>Assessed</span><span className="category-percentage">35%</span></p>
                                    <p className="category-candidate">65</p>
                                </div>
                                <div className="funnel-overlay">
                                    <p className="upgrade">Upgrade for candidate assessment</p>
                                    <button className="btn btn-primary upgrade-btn" onClick={() => props.upgrade()}>{props.upgrading ? "Upgrading..." : "Upgrade"}</button>
                                </div>
                            </div>
                            <div className="funnel-category delievered">
                                <div className="category-progress" style={{ width: "20%" }}></div>
                                <div className="category-wrapper">
                                    <p><span>Delivered</span><span className="category-percentage">20%</span></p>
                                    <p className="category-candidate">65</p>
                                </div>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        </Fragment>
    )
}