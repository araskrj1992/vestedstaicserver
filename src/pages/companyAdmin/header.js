import React from "react";
import { NavLink } from "react-router-dom"


export default class Header extends React.Component {

    render() {
        const { companyDetails } = this.props
        return (
            <nav>
                <div className="company-menu-wrapper">
                    <NavLink
                        to="/"
                        exact
                        className="menu-item logo"
                        activeClassName="active"
                    >
                        <img src="/images/general/logo.svg" className="menu-logo" alt="Vested" />
                    </NavLink>
                    <div className="company-wrapper">
                        <div className="company-name">
                            <img className="company-logo" src={companyDetails.logoUrl} alt={companyDetails.companyName} />
                            <div>
                                <h4>{companyDetails.companyName}<img src="/images/companyAdmin/down.svg" alt="down" /></h4>
                                <h5>{companyDetails.package}</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        );
    }
}
