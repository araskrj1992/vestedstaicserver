import React from "react";
import { Row, Col, Button } from "react-bootstrap";
const JobFit = (props) => {
    return (
        <section className="dark jobfit" id="jobFits">
            <Row>
                <Col xs={12} md={12} className="text-center">
                    <h2 className="heading">What Job Fits You?</h2>
                </Col>
            </Row>
            <Row className="content-wrapper">
                <Col xs={12} md={6}>
                    <div className="job-content">
                        <h4 className="job-name">Accounting</h4>
                        <h5 className="applications"><img src="/images/jobSeekers/applicants.svg" alt="applicants" /><span>34</span> other candidates placed in the last 1 month</h5>
                        <p className="description">Create your unique accounting profile and get noticed by top companies. Show-off your accounting knowledge and find fulfilling work today!</p>
                        <a
                            className="btn btn-sign-up"
                            href="https://hire.withgoogle.com/public/jobs/getvestedio/view/P_AAAAAADAAGhIJiLzNyIRFk"
                            target="_blank"
                            rel="noopener noreferrer"
                        >Sign Up
                        </a>
                        {/* <Button className="btn-sign-up">Sign Up</Button> */}
                    </div>
                </Col>
                <Col xs={12} md={6}>
                    <div className="job-content">
                        <h4 className="job-name">Finance</h4>
                        <h5 className="applications"><img src="/images/jobSeekers/applicants.svg" alt="applicants" /><span>37</span> other candidates placed in the last 1 month</h5>
                        <p className="description">Create your unique finance profile and get noticed by top companies. Show-off your excel skills and financial aptitude and find fulfilling work today!</p>
                        <a
                            className="btn btn-sign-up"
                            href="https://hire.withgoogle.com/public/jobs/getvestedio/view/P_AAAAAADAAGhIyXzzV6Qr6Z"
                            target="_blank"
                            rel="noopener noreferrer"
                        >Sign Up
                        </a>
                    </div>
                </Col>
            </Row>
            <Row className="content-wrapper">
                <Col xs={12} md={6}>
                    <div className="job-content">
                        <h4 className="job-name">sales</h4>
                        <h5 className="applications"><img src="/images/jobSeekers/applicants.svg" alt="applicants" /><span>23</span> other candidates placed in the last 1 month</h5>
                        <p className="description">Create your unique sales profile and get noticed by top companies. Show off your prospecting, outreach, and closing techniques to find fulfilling work today!</p>
                        <a
                            className="btn btn-sign-up"
                            href="https://hire.withgoogle.com/public/jobs/getvestedio/view/P_AAAAAADAAGhCT4CIjbHswG"
                            target="_blank"
                            rel="noopener noreferrer"
                        >Sign Up
                        </a>
                    </div>
                </Col>
                <Col xs={12} md={6}>
                    <div className="job-content">
                        <h4 className="job-name">other</h4>
                        <h5 className="applications"><img src="/images/jobSeekers/applicants.svg" alt="applicants" /><span>28</span> other candidates placed in the last 1 month</h5>
                        <p className="description">Not in accounting, finance or sales?  Create your unique profile and let us know the types of roles you’re interested in!</p>
                        <a
                            className="btn btn-sign-up"
                            href="https://hire.withgoogle.com/public/jobs/getvestedio/view/P_AAAAAADAAGhBkWRLGO-fOt"
                            target="_blank"
                            rel="noopener noreferrer"
                        >Sign Up
                        </a>
                    </div>
                </Col>
            </Row>
            {/* <Button className="btn btn-primary viewall" onClick={() => props.toggleAllOpportunities()}>View All</Button> */}
        </section>
    );
}


export default JobFit;
