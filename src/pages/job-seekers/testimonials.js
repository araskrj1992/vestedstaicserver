import React from "react";
import { Row, Col } from "react-bootstrap";
const Testimonials = () => {
    return (
        <section className="testimonials">
            <Row>
                <Col xs={12} md={12} className="text-center">
                    <h2 className="heading">Testimonials</h2>
                </Col>
            </Row>
            <Row className="content-wrapper">
                <Col xs={12} sm={4} md={4}>
                    <div className="testimonial-content">
                        <img
                            src="/images/jobSeekers/nicola.png"
                            alt="Nicola M, Controller"
                            className="img-circle"
                        />
                        <h4 className="name">Nicola M.</h4>
                        <h5 className="designation">Controller</h5>
                        <p className="comment">Signing up for Vested was well worth the effort.  Once I completed my profile, it was immediately sent to employers and I had a new job within weeks</p>
                    </div>
                </Col>
                <Col xs={12} sm={4} md={4}>
                    <div className="testimonial-content">
                        <img
                            src="/images/jobSeekers/duane.png"
                            alt="Duane T., Accountant"
                            className="img-circle"
                        />
                        <h4 className="name">Duane T.</h4>
                        <h5 className="designation">Accountant</h5>
                        <p className="comment">Vested highlighted my skills and allowed me to stand out to potential employers.  I would 10/10 recommend Vested to other professionals</p>
                    </div>
                </Col>
                <Col xs={12} sm={4} md={4}>
                    <div className="testimonial-content last-testimonial">
                        <img
                            src="/images/jobSeekers/kamar.png"
                            alt="Kamar F., Sales Rep"
                            className="img-circle"
                        />
                        <h4 className="name">Kamar F.</h4>
                        <h5 className="designation">Sales Rep.</h5>
                        <p className="comment">My Vested Profile is very detailed and captures my skill sets and key attributes! Helped me find the perfect fit for myself!</p>
                    </div>
                </Col>
            </Row>
        </section>
    );
}


export default Testimonials;
