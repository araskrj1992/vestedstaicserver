import React from "react";
import { Modal, Row, Col, Button } from "react-bootstrap";
const AllOpportunities = (props) => {
    return (
        <Modal show={true} className="all-opportunities">
            <Modal.Header className="opportunity-header" >
                <Modal.Title>All Opportunities</Modal.Title>
                <img src="/images/jobSeekers/close.png" alt="close" onClick={() => props.toggleAllOpportunities()} />
            </Modal.Header>
            <Modal.Body className="opportunities-content">
                <div className="categories-wrapper">
                    <Button className="category active-category">Finance<span>4</span></Button>
                    <Button className="category">Technology<span>4</span></Button>
                    <Button className="category">Advertising<span>4</span></Button>
                </div>
                <div className="feature-job-wrapper">
                    <Row className="content-wrapper">
                        <Col xs={12} md={6}>
                            <div className="job-content">
                                <h4 className="job-name">Chief Finance Officer <span><img src="/images/careers/location.svg" alt="new york" />New York City, NY</span></h4>
                                <h5 className="company-name">Tesla Motors</h5>
                                <p className="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc molestie ex eu rutrum consectetur. </p>
                                <div className="btn-wrapper">
                                    <a
                                        className="btn btn-sign-up"
                                        href="https://hire.withgoogle.com/public/jobs/getvestedio/view/P_AAAAAADAAGhIJiLzNyIRFk"
                                        target="_blank"
                                        rel="noopener noreferrer"
                                    >apply
                        </a>
                                </div>
                            </div>
                        </Col>
                        <Col xs={12} md={6}>
                            <div className="job-content">
                                <h4 className="job-name">Chief Finance Officer <span><img src="/images/careers/location.svg" alt="new york" />New York City, NY</span></h4>
                                <h5 className="company-name">Tesla Motors</h5>
                                <p className="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc molestie ex eu rutrum consectetur. </p>
                                <div className="btn-wrapper">
                                    <a
                                        className="btn btn-sign-up"
                                        href="https://hire.withgoogle.com/public/jobs/getvestedio/view/P_AAAAAADAAGhIJiLzNyIRFk"
                                        target="_blank"
                                        rel="noopener noreferrer"
                                    >apply
                        </a>
                                </div>
                            </div>
                        </Col>
                    </Row>
                    <Row className="content-wrapper">
                        <Col xs={12} md={6}>
                            <div className="job-content">
                                <h4 className="job-name">Chief Finance Officer <span><img src="/images/careers/location.svg" alt="new york" />New York City, NY</span></h4>
                                <h5 className="company-name">Tesla Motors</h5>
                                <p className="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc molestie ex eu rutrum consectetur. </p>
                                <div className="btn-wrapper">
                                    <a
                                        className="btn btn-sign-up"
                                        href="https://hire.withgoogle.com/public/jobs/getvestedio/view/P_AAAAAADAAGhIJiLzNyIRFk"
                                        target="_blank"
                                        rel="noopener noreferrer"
                                    >apply
                        </a>
                                </div>
                            </div>
                        </Col>
                        <Col xs={12} md={6}>
                            <div className="job-content">
                                <h4 className="job-name">Chief Finance Officer <span><img src="/images/careers/location.svg" alt="new york" />New York City, NY</span></h4>
                                <h5 className="company-name">Tesla Motors</h5>
                                <p className="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc molestie ex eu rutrum consectetur. </p>
                                <div className="btn-wrapper">
                                    <a
                                        className="btn btn-sign-up"
                                        href="https://hire.withgoogle.com/public/jobs/getvestedio/view/P_AAAAAADAAGhIJiLzNyIRFk"
                                        target="_blank"
                                        rel="noopener noreferrer"
                                    >apply
                        </a>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </div>
            </Modal.Body>
        </Modal>
    );
}


export default AllOpportunities;
