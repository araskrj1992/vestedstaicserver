import React, { Fragment } from "react";
import WhatWho from "../common/whatWho";
import HowItWorks from "./howItWork";
import Company from "../common/company";
import Splash from "../common/splash";
import Testimonials from "./testimonials"
import JobFit from "./jobfit"
import AllOpportunities from "./allOpportunities"
import Meta from "../common/meta";



export default class JobSeekers extends React.Component {
    state = {
        allOpportunities: false
    }
    render() {
        let metaTitle = 'Vested Technology';
        let metaDescription = 'Find a Job You Love';
        let metaImage = window.location.origin + '/images/jobSeekers/jobSeekers.jpg';
        return (
            <Fragment>
                <section className="job-seekers">
                    <Meta title={metaTitle} description={metaDescription} image={metaImage} />
                    <Splash
                        custClass="job-seekers-splash"
                        heading="Find A Job You Love"
                        content="Show Off Your Skills.  Stand Out.  Find Meaningful Work."
                        buttonText="Get Started"
                        btnStyle="primary"
                        bgImg="/images/jobSeekers/splash.png"
                        scrollId="jobFits"
                    />
                    <WhatWho
                        heading="How Vested Helps You"
                        text="Every candidate deserves a career they love. Vested helps you find fulfilling work, where you can leverage your skills and look forward to going to work everyday. Vested’s unique approach makes this all possible."
                        dark={true}
                    />
                    <HowItWorks />
                    <div className="what-vested-do">
                        <p>Your unique profile helps you stand apart from your peers while showing where your skills rank among them.  Vested matches your skills, personality, and cultural references to help you find fulfilling work.</p>
                    </div>
                    <hr />
                    <Testimonials />
                    <JobFit toggleAllOpportunities={() => this.setState({ allOpportunities: true })} />
                    <Company />
                </section>
                {this.state.allOpportunities &&
                    <AllOpportunities
                        show={this.state.allOpportunities}
                        toggleAllOpportunities={() => this.setState({ allOpportunities: false })}
                    />}
            </Fragment>
        );
    };
};

