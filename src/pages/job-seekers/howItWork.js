import React, { Component } from "react";
// import { Link } from "react-router-dom";
import { Row, Col, Button } from "react-bootstrap";

class HowItWorks extends Component {
    render() {
        return (
            <section className="howToWork">
                <Row>
                    <h2 className="heading mt-0 pb-5">How It Works</h2>
                    <div className="timeline">
                        <div className="center-box">
                            <div className="vertical-line">
                                <div className="part part1"></div>
                                <div className="part part2"></div>
                                <div className="part part3"></div>
                                <div className="part part4"></div>
                            </div>
                        </div>
                        <Row>
                            <Col sm={12} md={6}>
                                <div
                                    className="timeline-item right"
                                >
                                    <div className="content">
                                        <div className="company-size">
                                            <h4>Company Size</h4>
                                            <Button className="startup">Startup</Button>
                                            <Button>SMB</Button>
                                            <Button>Enterprise</Button>
                                        </div>
                                    </div>
                                </div>
                            </Col>
                            <Col sm={12} md={6}>
                                <div
                                    className="timeline-item"
                                >
                                    <div className="content">
                                        <h3>Share Your Preferences</h3>
                                        <p>Tell us what you’re looking for in your next employer</p>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                        <Row>
                            <Col sm={12} md={6}>
                                <div
                                    className="timeline-item right"
                                >
                                    <div className="content">
                                        <h3>Showcase Yourself</h3>
                                        <p>Take our skills challenge and share your personality to ensure the best fit</p>
                                    </div>
                                </div>
                            </Col>
                            <Col sm={12} md={6}>
                                <div
                                    className="timeline-item"
                                >
                                    <div className="content">
                                        <div className="skills-match">
                                            <h4>Skills Match</h4>
                                            <p>You scored better than <span>95%</span> of candidates on the Vested platform </p>
                                        </div>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                        <Row>
                            <Col sm={12} md={6}>
                                <div
                                    className="timeline-item right"
                                >
                                    <div className="content">
                                        <img className="acc-manager" src="/images/jobSeekers/work_acm.png" alt="account manager" />
                                    </div>
                                </div>
                            </Col>
                            <Col sm={12} md={6}>
                                <div
                                    className="timeline-item"
                                >
                                    <div className="content">
                                        <h3>Meet Us</h3>
                                        <p>Meet with our talent advisor team to discuss the next steps in your career</p>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                        <Row>
                            <Col sm={12} md={6}>
                                <div
                                    className="timeline-item right"
                                >
                                    <div className="content">
                                        <h3>Let Employers Discover You</h3>
                                        <p>Your unique qualities are assembled into an exclusive profile designed to help you stand out and land that next job</p>
                                    </div>
                                </div>
                            </Col>
                            <Col sm={12} md={6}>
                                <div
                                    className="timeline-item"
                                >
                                    <div className="content">
                                        <img src="/images/jobSeekers/work_skill.png" alt="skill manager" />
                                    </div>
                                </div>
                            </Col>
                        </Row>
                        {/* {Timeline.map((item, index) => {
                                return (
                                    <div
                                        key={index}
                                        className={
                                            index % 2 === 0
                                                ? "item right"
                                                : "item left"
                                        }
                                    >
                                        <div className="content">
                                            <h3>{item.heading}</h3>
                                            <p>{item.content}</p>
                                        </div>
                                    </div>
                                );
                            })} */}
                    </div>
                </Row>
            </section>
        );
    }
}

export default HowItWorks;
