import React from "react";
import { Row, Col } from "react-bootstrap";

const benefits = [
    {
        heading: "Enjoy quality time",
        description: "Take as much time as you need away from work to recharge, travel, take care of personal stuff, or spend time with family and friends."
    },
    {
        heading: "Give back",
        description: "We value giving back to the community and support time off to support the charitable causes you feel close to."
    },
    {
        heading: "Invest In You",
        description: "We offer extensive opportunities for personal and professional development. We’ll support you in doing what you love!"
    },
    {
        heading: "Support your loved ones",
        description: "We understand that your loved ones come first and and we offer generous parental leave policies and other support."
    }
]

const BenefitsPerks = () => {
    return (
        <section className="benefitsPerks">
            <Row>
                <Col xs={12} md={12} className="text-center">
                    <h2 className="heading">Benefits And Perks</h2>
                </Col>
            </Row>
            <Row className="content-wrapper">
                {
                    benefits.map((item, i) => {
                        return (
                            <Col xs={12} sm={6} md={6} key={"benefits" + i}>
                                <div className="benefits-content">
                                    <h4 className="benefits-name">{item.heading}</h4>
                                    <p className="description">{item.description}</p>
                                </div>
                            </Col>
                        )
                    })
                }

            </Row>
        </section>
    );
};

export default BenefitsPerks;



