import React from "react";
import { Row, Col } from "react-bootstrap";

const teamMembers = [
    {
        name: "Karoon Monfared",
        designation: "Founder & Partner",
        linkedinLink: "https://www.linkedin.com/in/karoon/",
        imageSrc: "/images/brand/moka/team/karoon-monfared.png"
    },
    {
        name: "Moncef Zizi",
        designation: "Founder & Partner",
        linkedinLink: "https://www.linkedin.com/in/moncefzizi/",
        imageSrc: "/images/brand/moka/team/moncef-zizi.png"
    },
    {
        name: "Todd Schiller",
        designation: "Engineering & Partner",
        linkedinLink: "https://www.linkedin.com/in/tschiller/",
        imageSrc: "/images/brand/moka/team/todd-schiller.png"
    }
]


const Team = () => {
    return (
        <section className="team mokaTeam" >
            <Row>
                <Col xs={12} md={12} className="text-center">
                    <h2 className="heading">Meet Our Team</h2>
                </Col>
            </Row>
            <Row className="content-wrapper">
                {
                    teamMembers.map((item, i) => {
                        return (
                            <Col xs={12} sm={6} md={4} key={"member" + i}>
                                <div className="team-content">
                                    <img src={item.imageSrc} alt={item.name + " moka"} />
                                    <div className="overlay">
                                        <div className="member-details">
                                            <h3>{item.name}</h3>
                                            <h4>{item.designation}</h4>
                                            <a href={item.linkedinLink} target="_blank" rel="noopener noreferrer"><span className="social-wrapper"><img src="/images/careers/linkedin.svg" alt="linkedin" /></span></a>
                                        </div>
                                    </div>
                                </div>
                            </Col>
                        )
                    })
                }
            </Row>
        </section>
    );
}


export default Team;
