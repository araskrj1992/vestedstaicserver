import React from "react";
import { Link } from "react-router-dom"
import { Row, Col } from "react-bootstrap";


const FeaturedJobs = () => {
    return (
        <section className="moka-featuredJobs" id="featuredJobs">
            <Row>
                <Col xs={12} md={12} className="text-center">
                    <h2 className="heading">Featured Jobs</h2>
                    <p className="sub-head">Check out our openings and apply today!</p>
                </Col>
            </Row>
            <Row className="content-wrapper row-eq-height">
                <Col sm={6} md={4}>
                    <div className="job-content">
                        <div className="content">
                            <h4 className="job-name">Vice President, Deployment Strategy</h4>
                            <p className="location">New York City, NY</p>
                            <p className="about-job">We’re looking for a ‘Partner’ level candidate, we want you to be the face of our organisation at various client meetings and events. You will also be responsible for contexualising business problems and strategizing the path forward for the company.</p>
                        </div>
                        <div className="btn-wrapper">
                            <Link
                                className="btn btn-sign-up"
                                to="/moka/jobs/vp-deployment"
                            >apply
                        </Link>
                        </div>
                    </div>
                </Col>
                <Col sm={6} md={4}>
                    <div className="job-content row-eq-height">
                        <div className="content">
                            <h4 className="job-name">senior software engineer</h4>
                            <p className="location">New York City, NY</p>
                            <p className="about-job">Your work is at the core of everything we build.  Help us develop complex enterprise software systems that scale across the largest multinational companies.</p>
                        </div>
                        <div className="btn-wrapper">
                            <Link
                                className="btn btn-sign-up"
                                to="/moka/jobs/sw-eng"
                            >apply
                        </Link>
                        </div>
                    </div>
                </Col>
                <Col sm={6} md={4}>
                    <div className="job-content row-eq-height">
                        <div className="content">
                            <h4 className="job-name">senior data engineer</h4>
                            <p className="location">New York City, NY</p>
                            <p className="about-job">We are looking for a Senior Data Engineer who shares a passion both for our mission and for working with big data.</p>
                        </div>
                        <div className="btn-wrapper">
                            <Link
                                className="btn btn-sign-up"
                                to="/moka/jobs/data-eng"
                            >apply
                        </Link>
                        </div>
                    </div>
                </Col>
                {/* <Col md={12}>
                    <div className="job-content">
                        <Row>
                            <Col xs={12} sm={8} md={8}>
                                <h4 className="job-name">Vice President, Deployment Strategy</h4>
                                <p className="job-type">Full Time</p>
                            </Col>
                            <Col xs={12} sm={4} md={4}>
                                <div className="location-salary"><span><img src="/images/brand/moka/location.svg" alt="new york" />New York City, NY</span></div>
                                <div className="location-salary"><span><img src="/images/brand/moka/salary.svg" alt="new york" />$70,000 - 80,000</span></div>
                            </Col>
                        </Row>
                        <h6 className="content-heading">job description</h6>
                        <p className="description">Moka is looking for a ‘Partner’ level candidate, we want you to be the face of our organisation at various client meetings and events. You will also be responsible for contexualising business problems and strategizing the path forward for Moka as a company. An analytical mindset, assertive personality and a data oriented approach to solving problems are some of the qualities we are looking for in the canidate.</p>
                        <h6 className="content-heading">about you</h6>
                        <ul>
                            <li>You are excited about rolling up your sleeves and to dive into the details of the data because that’s the core of our work</li>
                            <li>Excellent communication and presentation skills with attention to detail</li>
                            <li>5+ years of Consulting, Program Management,, Customer Success or Sales field experience</li>
                            <li>BA/BS (or MA/MS) or equivalent experience</li>
                        </ul>
                        <p className="description">Hit ‘Learn More’ if you want to know about the role!</p>
                        <div className="btn-wrapper">
                            <Link
                                className="btn btn-sign-up"
                                to="/moka/jobs/vp-deployment"
                            >Learn More
                        </Link>
                        </div>
                    </div>
                </Col> */}
            </Row>
        </section>
    );
}

export default FeaturedJobs;
