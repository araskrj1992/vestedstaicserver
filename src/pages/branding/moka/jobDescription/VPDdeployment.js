import React, { Fragment } from "react";
import { Link } from "react-router-dom"
import { Row, Col } from "react-bootstrap";
import Splash from "../../../common/splash";
import BrandMenu from "../../../common/brandMenu"
import Svg from "../../../common/svg";
import Meta from "../../../common/meta";

const whatwilldo = [
    "Quickly understand the MOKA Winning Model, the power and possibility it brings to our customers, and the significance of our market opportunity",
    "Go onsite and meet with clients to understand the critical questions they need to answer and locate their biggest pain points",
    "Incorporate the relevant datasets into the MOKA Winning Model while working with our engineers to integrate the data into a stable and extensible pipeline",
    "Create and lead training sessions to ensure that the product is meeting the needs of our clients and is being used widely enough to have concrete impact on our Clients’ operations",
    "Present the results of our work and proposals for future work to audiences ranging from analysts to C-suite executives",
    "Build and deliver demos to new and existing customers",
    "Scope out potential engagements in new industries and in increasingly expanding locations around the world"
]

const aboutYou = [
    "You are excited about rolling up your sleeves and to dive into the details of the data because that’s the core of our work",
    "5+ years of Consulting, Program Management, Customer Success or Sales field experience",
    "A deep understanding of the role Account Executives, Customer Success Managers and Consultants play in the SaaS world",
    "BA/BS (or MA/MS) or equivalent experience",
    "Excellent communication and presentation skills with attention to detail",
    "Executive presence, and confidence to challenge and question",
    "A proven record of project management and multiple examples of driving strategic field initiatives",
    "A data-driven approach, and a commitment to process optimization",
    "Experience with programming, scripting or statistical packages a plus (Python, SQL)",
    "Ability to travel up to 30% required"
]
const VPDeployment = ({ contactToggle }) => {
    let metaTitle = 'VP Deployment, MOKA';
    let metaDescription = 'MOKA seeks a highly motivated VP to join the team.  Powered by Vested Technology.';
    let metaImage = window.location.origin + '/images/brand/moka/jobDescription/meta.png';
    return (
        <Fragment>
            <section className="job-description">
                <Meta title={metaTitle} description={metaDescription} image={metaImage} />
                <BrandMenu
                    iconSrc="/images/brand/moka/logoWhite.svg"
                    iconBgSrc="/images/brand/endeavor/logo.svg"
                    btnLink="https://hire.withgoogle.com/public/jobs/getvestedio/view/P_AAAAAADAAGhLgRM3YnS5tV"
                    iconLink="/moka"
                    btnText="Apply Now"
                />
                <Splash
                    custClass="moka-splash"
                    heading="Vice President, Deployment Strategy"
                    content="FULL TIME - NEW YORK"
                    bgImg="/images/brand/moka/splash.png"
                />
                <div className="aboutJob">
                    <h4>About MOKA</h4>
                    <p>MOKA Analytics leverages machine intelligence to enhance business decision-making.  Our suite of proprietary technology tools are focused on driving smarter, faster, and better integrated decisions within large organizations, disrupting the traditional strategy services delivery model.</p>
                    <p>Our flagship product, the MOKA Winning Model, is the preferred strategy analytics platform for some of the world's largest consumer companies.  Simply stated, our clients use the Winning Model to make decisions on how to best allocate resources to growth initiatives.   If you are are excited about solving problems in a business context, shaping an early stage product, and working with a team of top performers, let's start a conversation.</p>
                    <Link to="/moka">Learn More <Svg src="/images/brand/jobDescription/know-more-arrow.svg" /></Link>
                </div>
                <img className="jobStripPic" src="/images/brand/moka/jobDescription/aboutMoka.png" alt="about moka" />
                <Row className="content-wrapper">
                    <Col xs={12} sm={12} md={12}>
                        <h4 className="aboutRole">About The Role</h4>
                        <p className="aboutRoleDes">We are looking for a deployment lead that will help contexualize client business problems and create the appropriate product roadmap for MOKA.  We are looking for someone with an analytical mindset, assertive personality and a data-oriented approach to solving problems.</p>
                    </Col>
                    <Col xs={12} sm={8} md={8}>
                        <Content data={whatwilldo} title="What You’ll Do" />
                        <Content data={aboutYou} title="About You" />
                        <Row className="benefit-culture-wrapper">
                            <Col xs={12} sm={12} md={6}>
                                <div className="benefit-culture">
                                    <h4>Perks & Benefits</h4>
                                    <p>In addition to an engaging and dynamic work environment that promotes professional development and self-growth, we offer a lot of perks too: medical and flexible paid time off.</p>
                                </div>
                            </Col>
                            <Col xs={12} sm={12} md={6}>
                                <div className="benefit-culture">
                                    <h4>MOKA Culture</h4>
                                    <p>We are proud to be an equal opportunity employer and are committed to providing a workplace free of harassment and discrimination.  All aspects of consideration for employment with MOKA Analytics are governed on the basis of merit, competence and qualifications without regard to race, color, religion, sex, national origin, age, disability, veteran status, sexual orientation, or any other category protected by federal, state, or local law.</p>
                                </div>
                            </Col>
                        </Row>
                    </Col>
                    <Col xs={12} sm={4} md={4} className="sticky">
                        <div className="role-information">
                            <h3>Role Information</h3>
                            <p><span><img src="/images/brand/jobDescription/time.png" alt="time" /></span>Full Time</p>
                            <p><span><img src="/images/brand/jobDescription/location.png" alt="location" /></span>New York, NY</p>
                            {/* <p><span><img src="/images/brand/jobDescription/money.png" alt="money" /></span>$70,000-80,000</p> */}
                            <p><span><img src="/images/brand/jobDescription/people.png" alt="people" /></span>5 - 10 People</p>
                            <p><span><img src="/images/brand/jobDescription/calendar.png" alt="calendar" /></span>Posted October 2018</p>
                            <h4>Meet Your Hiring Manager</h4>
                            <div className="hiring-manager">
                                <img className="managerImg" src="/images/brand/moka/team/karoon-monfared.png" alt="Karoon Monfared" />
                                <p>Karoon Monfared</p>
                                <a href="https://www.linkedin.com/in/karoon/" target="_blank" rel="noopener noreferrer"><img src="/images/brand/jobDescription/linkedin.png" alt="linkedin" /></a>
                            </div>
                            <div>
                                <a className="btn btn-default apply" href="https://hire.withgoogle.com/public/jobs/getvestedio/view/P_AAAAAADAAGhLgRM3YnS5tV" target="_blank" rel="noopener noreferrer">Apply Now</a>
                            </div>
                        </div>
                    </Col>
                </Row>
                <div className="talent dark">
                    <p>Join Vested’s Talent Community To Hear About Other Roles</p>
                    <button className="btn btn-default join-now" onClick={() => contactToggle()}>Join Now</button>
                </div>
            </section>
        </Fragment>
    );
};


const Content = ({ data, title }) => {
    return (
        <div className="content">
            <h3>{title}</h3>
            <ul>
                {
                    data.map((item, i) => {
                        return (
                            <li key={i}>{item}</li>
                        )
                    })
                }
            </ul>
        </div>
    )
}

export default VPDeployment;



