import React, { Fragment } from "react";
import { Link } from "react-router-dom"
import { Row, Col } from "react-bootstrap";
import Splash from "../../../common/splash";
import BrandMenu from "../../../common/brandMenu"
import Svg from "../../../common/svg";
import Meta from "../../../common/meta";

const whatwilldo = [
    "Be a key contributor to the development of our Winning Model platform",
    "Rapidly prototype features, and work with designers and business people to define the product",
    "Design data infrastructure, analytics and visualizations that transition seamlessly across levels of detail and through time",
    "Take a proactive role in defining product experience",
    "Develop an understanding of the consumer goods industry to support your judgement",
    "Incorporate a variety of information (e.g., application telemetry) to refine the user experience",
    "Improve team agility and productivity through investment in tools, process, and test infrastructure"
]

const aboutYou = [
    "Bachelor’s degree (or advanced degree) in Computer Science or related field",
    "Experience with our primary front-end stack: React, Redux, ES6, SASS, Jest",
    "Experience with our primary back-end stack: Python 3, Django, Celery, Redis, SQL",
    "Experience with UX test automation frameworks, e.g., Selenium",
    "Experience deploying to Azure, Heroku, AWS, or other PaaS",
    "Strong ability to communicate UX and architectural considerations",
    "Attention to detail, while not losing sight of the big picture",
    "Experience using multiple frameworks that informs your architecture and technology decisions",
    "Experience building maintainable systems that make use of modern testing and source code analysis tools"
]
const SeniorSoftwareEngineer = ({ contactToggle }) => {
    let metaTitle = 'Sr. Software Engineer, MOKA';
    let metaDescription = 'MOKA seeks a highly motivated Sr. Software Engineer to join the team.  Powered by Vested Technology.';
    let metaImage = window.location.origin + '/images/brand/moka/jobDescription/meta.png';
    return (
        <Fragment>
            <section className="job-description">
                <Meta title={metaTitle} description={metaDescription} image={metaImage} />
                <BrandMenu
                    iconSrc="/images/brand/moka/logoWhite.svg"
                    iconBgSrc="/images/brand/endeavor/logo.svg"
                    btnLink="https://hire.withgoogle.com/public/jobs/getvestedio/view/P_AAAAAADAAGhAesM4co1kDp"
                    iconLink="/moka"
                    btnText="Apply Now"
                />
                <Splash
                    custClass="moka-splash"
                    heading="Senior Software Engineer"
                    content="FULL TIME - NEW YORK"
                    bgImg="/images/brand/moka/splash.png"
                />
                <div className="aboutJob">
                    <h4>About MOKA</h4>
                    <p>MOKA Analytics leverages machine intelligence to enhance business decision-making.  Our suite of proprietary technology tools are focused on driving smarter, faster, and better integrated decisions within large organizations, disrupting the traditional strategy services delivery model.</p>
                    <p>Our flagship product, the Winning Model, is the preferred strategy analytics platform for some of the world's largest consumer companies.  Simply stated, our clients use the Winning Model to make decisions on how to best allocate resources to growth initiatives.   If you are are excited about solving problems in a business context, shaping an early stage product, and working with a team of top performers, let's start a conversation.</p>
                    <Link to="/moka">Learn More <Svg src="/images/brand/jobDescription/know-more-arrow.svg" /></Link>
                </div>
                <img className="jobStripPic" src="/images/brand/moka/jobDescription/aboutMoka.png" alt="about moka" />
                <Row className="content-wrapper">
                    <Col xs={12} sm={12} md={12}>
                        <h4 className="aboutRole">About The Role</h4>
                        <p className="aboutRoleDes">Our engineers develop the platform that enable large multinational companies to make important strategic decisions.  We're looking for engineers who bring fresh ideas from all areas, including information retrieval, system design, security, artificial intelligence, and natural language processing.</p>
                    </Col>
                    <Col xs={12} sm={8} md={8}>
                        <Content data={whatwilldo} title="What You’ll Do" />
                        <Content data={aboutYou} title="About You" />
                        <Row className="benefit-culture-wrapper">
                            <Col xs={12} sm={12} md={6}>
                                <div className="benefit-culture">
                                    <h4>Perks & Benefits</h4>
                                    <p>In addition to an engaging and dynamic work environment that promotes professional development and self-growth, we offer a lot of perks too: medical and flexible paid time off.</p>
                                </div>
                            </Col>
                            <Col xs={12} sm={12} md={6}>
                                <div className="benefit-culture">
                                    <h4>MOKA Culture</h4>
                                    <p>We are proud to be an equal opportunity employer and are committed to providing a workplace free of harassment and discrimination.  All aspects of consideration for employment with MOKA Analytics are governed on the basis of merit, competence and qualifications without regard to race, color, religion, sex, national origin, age, disability, veteran status, sexual orientation, or any other category protected by federal, state, or local law.</p>
                                </div>
                            </Col>
                        </Row>
                    </Col>
                    <Col xs={12} sm={4} md={4} className="sticky">
                        <div className="role-information">
                            <h3>Role Information</h3>
                            <p><span><img src="/images/brand/jobDescription/time.png" alt="time" /></span>Full Time</p>
                            <p><span><img src="/images/brand/jobDescription/location.png" alt="location" /></span>New York, NY</p>
                            {/* <p><span><img src="/images/brand/jobDescription/money.png" alt="money" /></span>$70,000-80,000</p> */}
                            <p><span><img src="/images/brand/jobDescription/people.png" alt="people" /></span>5 - 10 People</p>
                            <p><span><img src="/images/brand/jobDescription/calendar.png" alt="calendar" /></span>Posted October 2018</p>
                            <h4>Meet Your Hiring Manager</h4>
                            <div className="hiring-manager">
                                <img className="managerImg" src="/images/brand/moka/team/todd-schiller.png" alt="Todd Schiller" />
                                <p>Todd Schiller</p>
                                <a href="https://www.linkedin.com/in/tschiller" target="_blank" rel="noopener noreferrer"><img src="/images/brand/jobDescription/linkedin.png" alt="linkedin" /></a>
                            </div>
                            <div>
                                <a className="btn btn-default apply" href="https://hire.withgoogle.com/public/jobs/getvestedio/view/P_AAAAAADAAGhAesM4co1kDp" target="_blank" rel="noopener noreferrer">Apply Now</a>
                            </div>
                        </div>
                    </Col>
                </Row>
                <div className="talent dark">
                    <p>Join Vested’s Talent Community To Hear About Other Roles</p>
                    <button className="btn btn-default join-now" onClick={() => contactToggle()}>Join Now</button>
                </div>
            </section>
        </Fragment>
    );
};


const Content = ({ data, title }) => {
    return (
        <div className="content">
            <h3>{title}</h3>
            <ul>
                {
                    data.map((item, i) => {
                        if (typeof (item) !== "object") {
                            return (
                                <li key={i}>{item}</li>
                            )
                        } else {
                            return (
                                <li key={i}>{item.title}
                                    <ul className="subContent" key="subcontent">
                                        {item.subContent.map((item, i) => {
                                            return (
                                                <li key={"subcontent" + i}>{item}</li>
                                            )
                                        })}</ul>
                                </li>
                            )
                        }
                    })
                }
            </ul>
        </div>
    )
}

export default SeniorSoftwareEngineer;



