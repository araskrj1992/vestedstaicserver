import React, { Fragment } from "react";
import WhatWho from "../../common/whatWho";
import Splash from "../../common/splash";
import Meta from "../../common/meta";
import BrandMenu from "../../common/brandMenu";
import BenefitsPerks from "./benefitsPerks"
import Team from "./team"
import ReasonToJoinMoka from "./reasonToJoinMoka"
import FeaturedJobs from "./featuredJobs"
import FrequentQuestions from "../../common/frequentQuestions"

const questions = [
    {
        title: "What is MOKA’s Mission?",
        answer: "Our mission is to scale business intuition with machine intelligence and data to drive faster, smarter, and more integrated strategic decision making for organizations."
    },
    {
        title: "What is MOKA Looking for in Employees?",
        answer: "We are looking for people who are creative problem solvers, technically-oriented, and impact driven.  MOKA is dedicated to building a diverse team that embraces each person’s unique strengths while cultivating personal responsibility and growth."
    },
    {
        title: "What is MOKA’s “Winning Model” ?",
        answer: "The Winning Model is an intelligent, enterprise-ready, strategic planning platform for global consumer companies. With the Winning Model, our clients arrive at smarter decisions, faster."
    }
]


const MokaBrand = () => {
    let metaTitle = 'MOKA';
    let metaDescription = 'Find A Better Company';
    let metaImage = window.location.origin + '/images/logo.svg';
    return (
        <Fragment>
            <section className="mokaBrand">
                <Meta title={metaTitle} description={metaDescription} image={metaImage} />
                <BrandMenu
                    iconSrc="/images/brand/moka/logoWhite.svg"
                    iconBgSrc="/images/brand/endeavor/logo.svg"
                    iconLink="/moka"
                    btnText="View Jobs"
                    scrollId="featuredJobs"
                />
                <Splash
                    custClass="moka-splash"
                    heading="Make a Difference at MOKA"
                    content="Amplify strategic decision-making for some of the world’s largest companies"
                    buttonText="View Jobs"
                    btnStyle="primary"
                    bgImg="/images/brand/moka/splash.png"
                    scrollId="featuredJobs"
                />
                <WhatWho
                    heading="About Us"
                    text="MOKA helps some of the worlds largest companies make better decisions by infusing business strategy with technology.  We offer business advisory services and technology to help companies achieve Practical Clarity and real world results."
                    dark={true}
                />
                <ReasonToJoinMoka />
                <Team />
                <BenefitsPerks />
                <FeaturedJobs />
                <FrequentQuestions questions={questions} />
            </section>
        </Fragment>
    );
};

export default MokaBrand;



