import React from "react";
import { Row, Col } from "react-bootstrap";

const teamMembers = [
    {
        title: "Create Real Client Value",
        description: "We’re a customer-focused company that blends product and strategic advisory to create value beyond what’s possible with traditional delivery models.  Our product, the Winning Model, influences executive’s decisions at global companies.",
        imageSrc: "/images/brand/moka/reasonToJoinMoka/client-value.png"
    },
    {
        title: "Build on a Strong Track Record",
        description: "Since its founding in 2014, MOKA has been self-funded and profitable. Our clients include Global 500 companies that have come to rely on our ability to exceed expectations.",
        imageSrc: "/images/brand/moka/reasonToJoinMoka/track-record.png"
    },
    {
        title: "Be Part of an Amazing Journey",
        description: "We’re setting out to rapidly expand the set of customers and sectors we serve. To accomplish this, we are assembling a core team powered by the best talent and raising Series A funding to accelerate growth.",
        imageSrc: "/images/brand/moka/reasonToJoinMoka/journey.png"
    },
    {
        title: "Solve a Variety of Challenges",
        description: "We are always on the hunt for creative problem solvers.  At MOKA you’ll gain a deep understanding of our customer’s business challenges and use that information to help us improve.",
        imageSrc: "/images/brand/moka/reasonToJoinMoka/solutions.png"
    },
    {
        title: "Live Your Values",
        description: "Our founders Karoon and Moncef started the company based on their experience as strategy consultants at McKinsey and as consumer tech entrepreneurs. At MOKA, we value diversity, personal responsibility, and personal development.",
        imageSrc: "/images/brand/moka/reasonToJoinMoka/values.png"
    }
]


const ReasonToJoinMoka = () => {
    return (
        <section className="reasonToJoinMoka" >
            <Row>
                <Col xs={12} md={12} className="text-center">
                    <h2 className="heading">5 Reasons To Join MOKA</h2>
                </Col>
            </Row>
            <Row className="content-wrapper">
                {
                    teamMembers.map((item, i) => {
                        return (
                            <Col xs={12} sm={6} md={4} key={"member" + i}>
                                <div className="reason-content">
                                    <img src={item.imageSrc} alt={item.name + " moka"} />
                                    <h3>{item.title}</h3>
                                    <p>{item.description}</p>
                                </div>
                            </Col>
                        )
                    })
                }
            </Row>
        </section>
    );
}


export default ReasonToJoinMoka;
