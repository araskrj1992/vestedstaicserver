import React, { Fragment } from "react";
import { Row, Col } from "react-bootstrap";
import Splash from "../../../common/splash";
import BrandMenu from "../../../common/brandMenu"
import Meta from "../../../common/meta";

const whatwilldo = [
    {
        title: "Diligence & Execution: You will manage all aspects of the due diligence and execution processes including",
        subContent: [
            "Valuation and financial modeling for deals and the overall portfolio",
            "Internal communication to the executive team on deal progress and investment highlights",
            "Managing deal intermediaries, including legal counsels, financing partners, third-party diligence vendors and internal operations"
        ]
    },
    "Sourcing: You will lead Endeavor's efforts related to the sourcing and execution of new investment opportunities in the private education sector",
    "Advisory: Identify processes to aid in the due diligence and integration of acquisitions (pre and post-100 days)",
    "Reporting: Set acquisition KPIs post-acquisition and communicate progress internally and to our investors"
]

const aboutYou = [
    "B.S. in Accounting, Finance or Business Administration",
    "Strong Knowledge of Accounting Principles, GAAP and research skills",
    "At least 3 years’ experience in transaction advisory or Investment Banking is preferred",
    "Analytical and problem-solving skills",
    "Experience with ERP systems",
    "Solid written and oral communication, organizational and planning skills",
    "Ability to manage multiple, diverse projects and personnel concurrently",
    "Nice to haves: CPA or CFA",
]
const JobDescription = ({ contactToggle }) => {
    let metaTitle = 'Endeavor Schools';
    let metaDescription = 'Endeavor Schools; Corporate Development Associate.  Presented by Vested';
    let metaImage = window.location.origin + '/images/og.png';
    return (
        <Fragment>
            <section className="job-description endeavor-job">
                <Meta title={metaTitle} description={metaDescription} image={metaImage} />
                <BrandMenu
                    iconSrc="/images/brand/endeavor/logoWhite.png"
                    iconBgSrc="/images/brand/endeavor/end-logo.png"
                    btnLink="https://hire.withgoogle.com/public/jobs/getvestedio/view/P_AAAAAADAAGhDde9j-iKi2a"
                    btnText="Apply Now"
                    customClass="endeavor-nav"
                />
                <Splash
                    custClass="endeavor-splash"
                    heading="Corporate Development Lead"
                    content="FULL TIME - MIAMI"
                    bgImg="/images/brand/endeavor/splash.png"
                />
                <div className="aboutJob">
                    <h4>About Endeavor</h4>
                    <p>Come help us make a difference by shaping the future of our students! At Endeavor Schools, we are lifelong learners and make it our mission to help others develop into their full potential so the children we serve can thrive.  The opportunity to support the growth of our students and team members is what gets us out of bed in the morning. </p>
                    <p>Endeavor is a stable and growing family of private schools, spanning nine states committed to creating better education for a better world. By providing our schools the tools and resources needed to create optimal teaching and learning environments we make significant, lasting, positive differences in the communities we serve and that begins, and ends, with our people.</p>
                </div>
                <img className="jobStripPic" src="/images/brand/endeavor/aboutEndeavor.png" alt="about endeavor" />
                <Row className="content-wrapper">
                    <Col xs={12} sm={12} md={12}>
                        <h4 className="aboutRole">About The Role</h4>
                        <p className="aboutRoleDes">We’re growing fast and closing 10+ acquisitions per year due to our strong balance sheet with lots of opportunity for future growth too.  In other words, capital is not our constraint!  We’re looking for a hungry, humble and smart team member to to grow Endeavor School’s rapidly growing portfolio of operating and real estate assets.  Reporting directly to the CEO and CFO, this is a high-profile role where you will work closely with Endeavor’s executive team and its private equity backer (Leeds Equity Partners).</p>
                    </Col>
                    <Col xs={12} sm={8} md={8}>
                        <Content data={whatwilldo} title="What You’ll Do" />
                        <Content data={aboutYou} title="About You" />
                        <Row className="benefit-culture-wrapper">
                            <Col xs={12} sm={12} md={6}>
                                <div className="benefit-culture">
                                    <h4>Perks & Benefits</h4>
                                    <p>In addition to an engaging and dynamic work environment that promotes professional development and self-growth, we offer a lot of perks too: medical, dental, and vision insurance, 401(k) retirement savings plans, short-term disability, life insurance, flexible paid time off, education scholarships, and faculty tuition discounts.</p>
                                </div>
                            </Col>
                            <Col xs={12} sm={12} md={6}>
                                <div className="benefit-culture">
                                    <h4>Endeavor Culture</h4>
                                    <p>At Endeavor Schools, our pursuit of building a world-class family of private schools is guided by our core values. We seek to hold ourselves and each of our team members accountable for demonstrating them every day. After all, it is our core values that define our aspiration of how we interact together as teammates, as well as with the children, families and communities that we serve. Our core values are: 1) Recognition, 2) Excellence, 3) Service, 4) Positivity, 5) Ethics, 6) Collaboration, 7) Trust.</p>
                                </div>
                            </Col>
                        </Row>
                    </Col>
                    <Col xs={12} sm={4} md={4} className="sticky">
                        <div className="role-information">
                            <h3>Role Information</h3>
                            <p><span><img src="/images/brand/jobDescription/time.png" alt="time" /></span>Full Time</p>
                            <p><span><img src="/images/brand/jobDescription/location.png" alt="location" /></span>Miami, FL</p>
                            <p><span><img src="/images/brand/jobDescription/people.png" alt="people" /></span>50 - 100 People</p>
                            <p><span><img src="/images/brand/jobDescription/calendar.png" alt="calendar" /></span>Posted October-18</p>
                            <h4>Meet Your Hiring Manager</h4>
                            <div className="hiring-manager">
                                <img src="/images/brand/endeavor/joe.png" alt="Joseph O'Connel" />
                                <p>Joseph O’Connell</p>
                                <a href="https://www.linkedin.com/in/joe-o-connell-1062281/" target="_blank" rel="noopener noreferrer"><img src="/images/brand/jobDescription/linkedin.png" alt="linkedin" /></a>
                            </div>
                            <div>
                                <a className="btn btn-default apply" href="https://hire.withgoogle.com/public/jobs/getvestedio/view/P_AAAAAADAAGhDde9j-iKi2a" target="_blank" rel="noopener noreferrer">Apply Now</a>
                            </div>
                        </div>
                    </Col>
                </Row>
                <div className="talent dark">
                    <p>Join Vested’s Talent Community To Hear About Other Roles</p>
                    <button className="btn btn-default join-now" onClick={() => contactToggle()}>Join Now</button>
                </div>
            </section>
        </Fragment>
    );
};


const Content = ({ data, title }) => {
    return (
        <div className="content">
            <h3>{title}</h3>
            <ul>
                {
                    data.map((item, i) => {
                        if (typeof (item) !== "object") {
                            return (
                                <li key={i}>{item}</li>
                            )
                        } else {
                            return (
                                <li key={i}>{item.title}
                                    <ul className="subContent" key="subcontent">
                                        {item.subContent.map((item, i) => {
                                            return (
                                                <li key={"subcontent" + i}>{item}</li>
                                            )
                                        })}</ul>
                                </li>
                            )
                        }
                    })
                }
            </ul>
        </div>
    )
}

export default JobDescription;



