import React from "react";
import { Row, Col } from "react-bootstrap";
const Advantage = () => {
    return (
        <section className="advantage">
            <Row>
                <Col xs={12} md={12} className="text-center">

                    <h2 className="heading">The Vested Advantage</h2>
                </Col>
            </Row>
            <Row className="content-wrapper">
                <Col xs={12} sm={4} md={4}>
                    <div className="advantage-content">
                        <img src="/images/services/advantage1.png" alt="advantage" />
                        <h3>Average savings using Vested</h3>
                    </div>
                </Col>
                <Col xs={12} sm={4} md={4}>
                    <div className="advantage-content">
                        <img src="/images/services/advantage2.png" alt="advantage" />
                        <h3>More qualified candidates added to your funnel</h3>
                    </div>
                </Col>
                <Col xs={12} sm={4} md={4}>
                    <div className="advantage-content">
                        <img src="/images/services/advantage3.png" alt="advantage" />
                        <h3>Reduction in time-to-fill</h3>
                    </div>
                </Col>
            </Row>
        </section>
    );
}


export default Advantage;
