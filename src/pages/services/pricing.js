import React from "react";
import { Row, Col } from "react-bootstrap";

const content = [
    {
        heading: "Passive Candidate Sourcing",
        image: "/images/services/cost1.png",
        upfrontCost: "500",
        percentage: 5,
        features: [
            "Branded PDF for candidates",
            "Identification of your ideal talent pool",
            "Outreach to relevant candidates",
            "Engaged talent sent directly to you"
        ]
    },
    {
        heading: "Sourcing + Resume Filter",
        image: "/images/services/cost2.png",
        upfrontCost: "750",
        percentage: 7.5,
        features: [
            "Branded Company Page",
            "Identification of your ideal talent pool",
            "Outreach to relevant candidates",
            "Resumes collected from engaged candidates",
            "Resume screened for role fit, ranked, and sent to you",
            "Dashboard to follow the process and benchmark your results"
        ]
    },
    {
        heading: "Sourcing + Screening",
        image: "/images/services/cost3.png",
        upfrontCost: "1,000",
        percentage: 10,
        features: [
            "Branded Company Page",
            "Identification of your ideal talent pool",
            "Outreach to relevant candidates",
            "Resumes collected from engaged candidates",
            "Resume screened for role fit and ranked",
            "Dashboard to follow the process and benchmark your results",
            "Custom phone screening with detailed notes",
            "Screened candidate profile ranked and sent to you"
        ]
    },
    {
        heading: "Sourcing + Screening + Assessing",
        image: "/images/services/cost4.png",
        upfrontCost: "1,500",
        percentage: 15,
        features: [
            "Branded Company Page",
            "Identification of your ideal talent pool",
            "Outreach to relevant candidates",
            "Resumes collected from engaged candidates",
            "Resume screened for role fit and ranked",
            "Dashboard to follow the process and benchmark your results",
            "Custom phone screening with detailed notes",
            "Screened candidate profile ranked and sent to you",
            "Access to suite of culture, personality assessment tools",
            "Dedicated recruitment consultant"
        ]
    }
]
const Pricing = (props) => {
    return (
        <section className="pricing" id="pricing">
            <Row>
                <Col xs={12} md={12} className="text-center">
                    <h2 className="heading">Simple, Transparent & Low Cost</h2>
                    <h3 className="sub-heading">Start Hiring Today</h3>
                </Col>
            </Row>
            <Row className="content-wrapper">
                {content.map((item, i) => {
                    return (
                        <Col xs={12} sm={6} md={6} lg={3} key={`pricing${i}`} className="wrapper">
                            <div className="pricing-content">
                                <hr className="upper-border" />
                                <h3>{item.heading}</h3>
                                {/* <img src={item.image} alt="costing" /> */}
                                <div className="costing-wrapper">
                                    <div className="costing-percentage">
                                        {item.percentage}
                                        <p>
                                            <span>%</span>
                                            <span className="salary">first year</span>
                                            <span className="salary">salary</span>
                                        </p>
                                    </div>
                                </div>
                                <h4 className="cost">An Upfront Cost Of <span>${item.upfrontCost}</span></h4>
                                <ul>
                                    {item.features.map((data, i) => <li key={`feature${i}`}>{data}</li>)}
                                </ul>
                                <button className="btn btn-primary get-started" onClick={() => props.sendPricing(item)}>Get Started</button>
                            </div>
                        </Col>
                    )
                })}
            </Row>
        </section>
    );
}


export default Pricing;
