import React from "react";
import { Row, Col } from "react-bootstrap";
const HowItWorks = () => {
    return (
        <section className="howitworks" >
            <Row>
                <Col xs={12} md={12} className="text-center">
                    <h2 className="heading">How It Works</h2>
                </Col>
            </Row>
            <Row className="content-wrapper">
                <Col xs={12} sm={6} md={6}>
                    <div className="howitworks-content">
                        <h3>BUILD A ROBUST CANDIDATE PIPELINE</h3>
                        <p>Our unique technology finds and engages qualified candidates</p>
                    </div>
                </Col>
                <Col xs={12} sm={6} md={6}>
                    <div className="howitworks-content">
                        <h3>BREAK OPENINGS INTO REQUIRED SKILLS</h3>
                        <p>Our algorithms identify talent based on several key metrics</p>
                    </div>
                </Col>
            </Row>
            <Row className="content-wrapper">
                <Col xs={12} sm={6} md={6}>
                    <div className="howitworks-content">
                        <h3>QUALIFY TOP-TIER TALENT</h3>
                        <p>Candidates are screened using qualitative and quantitative assessments that maximize impact and reduce turnover</p>
                    </div>
                </Col>
                <Col xs={12} sm={6} md={6}>
                    <div className="howitworks-content">
                        <h3>BEST-IN-CLASS BRANDING</h3>
                        <p>Create a brand-driven career site to wow and convert talent</p>
                    </div>
                </Col>
            </Row>
        </section>
    );
}


export default HowItWorks;
