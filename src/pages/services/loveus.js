import React from "react";
import { Row, Col } from "react-bootstrap";
const LoveUs = () => {
    return (
        <section className="dark loveus" >
            <Row>
                <Col xs={12} md={12} className="text-center">
                    <h2 className="heading"> Why You’ll Love Us</h2>
                </Col>
            </Row>
            <Row className="content-wrapper">
                <Col xs={12} sm={6} md={4}>
                    <div className="loveus-content">
                        <img src="/images/services/faster.svg" alt="faster cheaper" />
                        <h3>ALWAYS FASTER & CHEAPER</h3>
                        <p>Our software provides access to top-tier candidates at a fraction of traditional third party recruiter costs</p>
                    </div>
                </Col>
                <Col xs={12} sm={6} md={4}>
                    <div className="loveus-content">
                        <img src="/images/services/sacrifice.svg" alt="sacrifice" />
                        <h3>NEVER SACRIFICE QUALITY</h3>
                        <p>A combination of technology and human touch delivers a world class recruiting experience</p>
                    </div>
                </Col>
                <Col xs={12} sm={6} md={4}>
                    <div className="loveus-content">
                        <img src="/images/services/customized.svg" alt="customized" />
                        <h3>CUSTOMIZED RECRUITMENT FLOW</h3>
                        <p>We work with you to create personality, cultural and skills assessments to ensure candidates are the best fit</p>
                    </div>
                </Col>
            </Row>
        </section>
    );
}


export default LoveUs;
