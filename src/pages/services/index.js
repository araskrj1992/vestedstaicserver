import React, { Fragment } from "react";
import Splash from "../common/splash";
import Company from "../common/company";
import HowItWorks from "./howitworks"
import Meta from "../common/meta";
import LoveUs from "./loveus";
import Advantage from "./advantage"
import Pricing from "./pricing"
import PricingContact from "./pricingContact"
import FormSuccess from '../common/successModal'

export default class Services extends React.Component {

    state = {
        pricingForm: false,
        formSendSuccess: false,
        pricingDetails: {}
    }

    sendPricing(data) {
        this.setState({
            pricingForm: true,
            pricingDetails: data
        })
    }

    render() {
        let metaTitle = 'Vested Technology';
        let metaDescription = 'Your Automated Staffing Solution';
        let metaImage = window.location.origin + '/images/services/services.jpg';
        return (
            <Fragment>
                <section className="jobs">
                    <Meta title={metaTitle} description={metaDescription} image={metaImage} />
                    <Splash
                        custClass="services-splash"
                        heading="The First Automated Staffing Solution"
                        content="Source, screen and assess talent using THE lowest cost recruiting provider"
                        buttonText="Get Started"
                        btnStyle="primary"
                        bgImg="/images/services/splash.png"
                        scrollId="pricing"
                    />
                    <HowItWorks />
                    <LoveUs />
                    <Advantage />
                    <Company />
                    <Pricing sendPricing={(data) => this.sendPricing(data)} />
                    <section className="dark reach-out">
                        <h2 className="heading">Reach Out To Us</h2>
                        <p>Looking for more information or to get started?  Please share some information about you and we’ll be in touch within <span>ONE</span> day.</p>
                        <button className="btn btn-primary contact" onClick={() => this.props.contactToggle()}>Contact Us</button>
                    </section>
                    <div style={{ padding: '20px' }}></div>
                </section>
                {this.state.pricingForm &&
                    <PricingContact
                        pricingForm={this.state.pricingForm}
                        contactToggle={() => this.setState({ pricingForm: false })}
                        pricingDetails={this.state.pricingDetails}
                        successPopup={() => this.setState({ formSendSuccess: !this.state.formSendSuccess })}
                    />
                }
                {this.state.formSendSuccess &&
                    <FormSuccess
                        successPopup={() => this.setState({ formSendSuccess: !this.state.formSendSuccess })}
                        formSendSuccess={this.state.formSendSuccess}
                    />
                }
            </Fragment>
        );
    }
};




