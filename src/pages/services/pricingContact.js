import React from "react";
import { Modal, Row, Col, ControlLabel, FormControl, HelpBlock, Button } from "react-bootstrap";
import validator from 'validator'
import { makeRequest } from '../../helper'

export default class ContactForm extends React.Component {

    state = {
        name: "",
        email: "",
        company: "",
        position: "",
        jobDescription: "",
        otherDescription: "",
        uploadfilePath: "",
        uploadfileName: "",
        error: {
            name: false,
            email: false,
            uploadfileName: false
        },
        isLoading: false,
        serverError: null
    }

    onSubmit = async (e) => {
        e.preventDefault()

        const { name, email, company, position, jobDescription, otherDescription, uploadfilePath, uploadfileName, error } = this.state
        const { pricingDetails } = this.props
        let pricingQuery = `${name} has queried about ${pricingDetails.heading} having an upfront cost of ${pricingDetails.upfrontCost} and a percentage of ${pricingDetails.percentage}`
        let data = JSON.stringify({
            name: name.trim(),
            email: email.trim(),
            company: company.trim(),
            position: position.trim(),
            jobDescription: jobDescription.trim(),
            otherDescription: otherDescription.trim(),
            pricingQuery: pricingQuery,
            uploadfilePath: uploadfilePath,
            uploadfileName: uploadfileName
        })
        Object.keys(this.state.error).forEach(item => {
            if (item === 'name') {
                if (validator.isEmpty(this.state[item].trim())) {
                    this.setError('name', true)
                } else {
                    this.setError('name', false)
                }
            }
            if (item === 'email') {
                if (!validator.isEmail(this.state[item].trim())) {
                    this.setError('email', true)
                } else {
                    this.setError('email', false)
                }
            }
            if (item === 'uploadfileName') {
                let fileName = this.state[item].trim().split(".")
                console.log(fileName)
                if (["pdf", "doc"].indexOf(fileName[fileName.length - 1].toLocaleLowerCase()) === -1) {
                    console.log("error")
                    this.setError('uploadfileName', true)
                } else {
                    this.setError('uploadfileName', false)
                }
            }
        })
        if (!error.name && !error.email && !error.uploadfileName) {
            this.setState({ isLoading: true })
            try {
                let fetchResult = await makeRequest('POST', '/sendPricingScheme', data)
                fetchResult = JSON.parse(fetchResult)
                if (fetchResult.success) {
                    this.setState({
                        isLoading: false,
                        name: "",
                        email: "",
                        company: "",
                        position: "",
                        jobDescription: "",
                        otherDescription: ""
                    })
                    this.props.contactToggle()
                    this.props.successPopup()
                }
            } catch (err) {
                this.setState({ isLoading: false, serverError: "There is some server error" })
            }

        }
    }

    setError(key, val) {
        let error = this.state.error
        error[key] = val
        this.setState({
            error
        })
    }

    handleChange(e) {
        console.log(e.target.files)

        if (e.target.type !== 'file') {
            this.setState({
                [e.target.name]: e.target.value
            })
        } else {
            let reader = new FileReader();
            let fileContent;
            reader.readAsDataURL(e.target.files[0]);
            reader.onload = function () {
                fileContent = reader.result;
            }
            this.setState({
                uploadfilePath: fileContent,
                uploadfileName: e.target.files[0].name
            })
        }

    }
    render() {
        const { serverError, isLoading } = this.state
        return (
            <Modal show={this.props.pricingForm} className="contact-form">
                <Modal.Header className="form-header" >
                    <Modal.Title>Get Started <span>Today</span></Modal.Title>
                    <p className="sub-heading">Share a bit of info and we’ll be in touch within 24 hours</p>
                </Modal.Header>
                <Modal.Body className="form-content">
                    <form onSubmit={this.onSubmit}>

                        <Row>
                            <Col xs={12} sm={12} md={12} >
                                {serverError && <span style={{ color: 'red', paddingBottom: '20px' }}>{serverError}</span>}
                            </Col>
                            <Col xs={12} sm={6} md={6} className="input-wrap">
                                <ControlLabel>Name<span className="required">*</span></ControlLabel>
                                <FormControl
                                    type="text"
                                    name="name"
                                    placeholder="Enter Name"
                                    value={this.state.name}
                                    onChange={(e) => this.handleChange(e)}
                                />
                                {this.state.error.name && <HelpBlock className="error">Name is required</HelpBlock>}
                            </Col>
                            <Col xs={12} sm={6} md={6} className="input-wrap">
                                <ControlLabel>Company Email<span className="required">*</span></ControlLabel>
                                <FormControl
                                    type="email"
                                    name="email"
                                    placeholder="Enter Company Email"
                                    value={this.state.email}
                                    onChange={(e) => this.handleChange(e)}
                                />
                                {this.state.error.email && <HelpBlock className="error">Email is required with valid email</HelpBlock>}
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={12} sm={6} md={6} className="input-wrap">
                                <ControlLabel>Company Name</ControlLabel>
                                <FormControl
                                    type="text"
                                    name="company"
                                    placeholder="Company Name"
                                    value={this.state.company}
                                    onChange={(e) => this.handleChange(e)}
                                />
                            </Col>
                            <Col xs={12} sm={6} md={6} className="input-wrap">
                                <ControlLabel>YOUR ROLE(S)</ControlLabel>
                                <FormControl
                                    type="text"
                                    name="position"
                                    placeholder="Enter Role(s)"
                                    value={this.state.position}
                                    onChange={(e) => this.handleChange(e)}
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={12} sm={12} md={12} className="input-wrap">
                                <ControlLabel>Job Description</ControlLabel>
                                <FormControl
                                    componentClass="textarea"
                                    rows="5"
                                    name="jobDescription"
                                    placeholder="Please Enter Your Job Description"
                                    value={this.state.jobDescription}
                                    onChange={(e) => this.handleChange(e)}
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={12} sm={12} md={12} className="input-wrap">
                                <ControlLabel>Other</ControlLabel>
                                <FormControl
                                    componentClass="textarea"
                                    rows="5"
                                    name="otherDescription"
                                    placeholder="Anything Else We Should Know?"
                                    value={this.state.otherDescription}
                                    onChange={(e) => this.handleChange(e)}
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={12} sm={12} md={12} className="input-wrap">
                                <ControlLabel>Job Description</ControlLabel>
                                <input type="file" name="uploadfile" onChange={(e) => this.handleChange(e)} placeholder="Upload Job Description" style={{ paddingLeft: 0 }} />
                                {this.state.error.uploadfileName && <HelpBlock className="error">Please upload pdf or doc file</HelpBlock>}
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={12} sm={12} md={12} className="btn-wrap">
                                {!isLoading && <Button className="submit-btn cancel" onClick={() => this.props.contactToggle()}>Cancel</Button>}
                                {!isLoading ? <Button className="submit-btn" type="submit" >Submit</Button> : <Button className="submit-btn" disabled={true} >Sending Query</Button>}
                            </Col>
                        </Row>
                    </form>
                </Modal.Body>
            </Modal>
        );
    }
}