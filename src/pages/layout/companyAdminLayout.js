import React, { Component, Fragment } from "react";
import CompanyAdminRoutes from '../layoutRoutes/companyAdminRoutes'

class CompanyAdminLayout extends Component {
    state = {
        contact: false,
        formSendSuccess: false
    };

    render() {
        return (
            <Fragment>
                <CompanyAdminRoutes {...this.props} contactToggle={() => this.setState({ contact: !this.state.contact })} />
            </Fragment>
        );
    }
}

export default CompanyAdminLayout;
