import React, { Component, Fragment } from "react";
import { Route, Switch, Redirect, withRouter } from "react-router-dom";
import BrandingRoutes from "../layoutRoutes/brandingRoutes";
import BrandFooter from "../common/brandFooter"
import JoinForm from '../common/joinForm'
import FormSuccess from '../common/successModal'

class BrandingLayout extends Component {
    state = {
        contact: false,
        formSendSuccess: false
    };


    render() {
        if (this.props.location.pathname === "/moka/jobs/partner-deployment-strategy-lead") {
            return <Redirect to="/moka/jobs/vp-deployment" />
        }
        return (
            <Fragment>
                <BrandingRoutes {...this.props} contactToggle={() => this.setState({ contact: !this.state.contact })} />
                <BrandFooter />
                <JoinForm
                    contactToggle={() => this.setState({ contact: !this.state.contact })}
                    successPopup={() => this.setState({ formSendSuccess: !this.state.formSendSuccess })}
                    contact={this.state.contact}
                />
                <FormSuccess
                    join
                    successPopup={() => this.setState({ formSendSuccess: !this.state.formSendSuccess })}
                    formSendSuccess={this.state.formSendSuccess}
                />
            </Fragment>
        );
    }
}

export default BrandingLayout;
