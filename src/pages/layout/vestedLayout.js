import React, { Component, Fragment } from "react";
// import { Timeline } from 'react-twitter-widgets'
import VestedRoutes from "../layoutRoutes/vestedRoutes";
import Menu from "../common/menu";
import Footer from "../common/footer";
import ContactForm from '../common/contactForm'
import FormSuccess from '../common/successModal'

class VestedLayout extends Component {
    state = {
        contact: false,
        formSendSuccess: false
    };

    render() {
        return (
            <Fragment>
                <Menu contactToggle={() => this.setState({ contact: !this.state.contact })} />
                <VestedRoutes {...this.props} contactToggle={() => this.setState({ contact: !this.state.contact })} />
                <Footer
                    contactToggle={() => this.setState({ contact: !this.state.contact })}
                />
                <ContactForm
                    contactToggle={() => this.setState({ contact: !this.state.contact })}
                    successPopup={() => this.setState({ formSendSuccess: !this.state.formSendSuccess })}
                    contact={this.state.contact}
                />
                <FormSuccess
                    successPopup={() => this.setState({ formSendSuccess: !this.state.formSendSuccess })}
                    formSendSuccess={this.state.formSendSuccess}
                />
            </Fragment>
        );
    }
}

export default VestedLayout;
