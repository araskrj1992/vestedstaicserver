import React from "react";
import { NavLink } from "react-router-dom";
import {
    Link as ScrollLink
} from 'react-scroll'
import Svg from "./svg";

export default class BrandMenu extends React.Component {

    state = {
        bgEnabled: false,
    };

    onScroll = () => {
        let bodyScrollTop =
            document.documentElement.scrollTop || document.body.scrollTop;
        if (bodyScrollTop > 80 && !this.state.bgEnabled) {
            this.setState({ bgEnabled: true });
        } else if (bodyScrollTop < 80 && this.state.bgEnabled) {
            this.setState({ bgEnabled: false });
        }
    };


    componentDidMount() {
        document.addEventListener("scroll", this.onScroll);
    }

    componentWillUnmount() {
        document.removeEventListener("scroll", this._onScroll);
    }

    createButton = () => {
        const props = this.props

        if (props.btnLink) {
            return (
                <a
                    href={props.btnLink}
                    target="_blank"
                    rel="noopener noreferrer"
                > <button className="btn btn-default contact" >{props.btnText}</button></a>
            )
        } else if (props.scrollId) {
            return (
                <ScrollLink className="btn btn-default contact"
                    to={props.scrollId} spy={true} smooth={true} duration={500} offset={-80}>
                    {props.btnText}
                </ScrollLink>
            )
        } else if (props.btnText) {
            return (
                <button className="btn btn-default contact" >{props.btnText}</button>
            )
        }
    }

    render() {
        let bgEnabledClassString = this.state.bgEnabled ? "bg-enabled" : "";
        let props = this.props;
        let customClass = props.customClass ? props.customClass : ""
        let iconSrc = props.iconSrc;
        if (this.state.bgEnabled) {
            iconSrc = props.iconBgSrc
        }
        return (
            <nav className={"brand-menu-wrapper " + bgEnabledClassString + " " + customClass}>
                {props.iconLink ? <NavLink to={props.iconLink}><Svg src={iconSrc} /></NavLink> : <img className="brand-logo" src={iconSrc} alt="brand icon" />}
                <div>
                    {this.createButton()}
                    {/* {props.btnText ? props.btnLink ? <a
                        href="https://twitter.com/VestedHR"
                        target="_blank"
                        rel="noopener noreferrer"
                    > <button className="btn btn-default contact" >{props.btnText}</button></a> : <Scroll type="id" element={props.scrollId} offset={-80}><button className="btn btn-default contact" >{props.btnText}</button></Scroll> : ''} */}

                </div>
            </nav>
        );
    }
}
