import React from "react";
import { Link } from "react-router-dom";
import { Row, Col } from "react-bootstrap";

const PolicyTerms = ({ brand }) => ({
    render() {
        return (
            <div className="policy">
                <Row className="row-wrap">
                    <Col xs={7} sm={6} md={6} className="col">
                        {!brand && <Link to="/"><img src="/images/general/logo.svg" alt="getvested" /></Link>}
                        <Link to="/terms-of-service">
                            <span>Terms and Conditions</span>
                        </Link>
                        <Link to="/privacy-policy">
                            <span>Privacy Policy</span>
                        </Link>
                    </Col>
                    <Col xs={5} sm={6} md={6} className="col">
                        <div className="copyright">
                            <span className="copyright-icon">©</span> Copyright Vested Technology 2018
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }
});

export default PolicyTerms;
