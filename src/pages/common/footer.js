import React from "react";
import { Link } from "react-router-dom";
import { Row, Col } from "react-bootstrap";
import Svg from "./svg";

export default class Footer extends React.Component {
    render() {
        return (
            <footer className="footer">
                <Links {...this.props} />
                <Policy />
            </footer>
        );
    }
}

// class Links extends React.Component {
//     render() {
//         return (
//             <div className="links">
//                 <div className="site-map">
//                     <Link to="/professionals">
//                         <span> Professionals </span>
//                     </Link>
//                     {/* <Link to="/employers">
//                         <span> Employers </span>
//                     </Link>
//                     <Link to="/consulting">
//                         <span> Consulting </span>
//                     </Link>
//                     <Link to="/temporary-jobs">
//                         <span> Temporary </span>
//                     </Link> */}
//                     {/* <a
//                         href="https://getvested.io/blog/"
//                         target="_blank"
//                         rel="noopener noreferrer"
//                     >
//                         <span> Blog </span>
//                     </a> */}
//                     {/* <Link to="/faq">
//                         <span> FAQ </span>
//                     </Link> */}
//                 </div>
// <div className="social">
//     <span> FOLLOW US </span>
//     <a
//         href="https://www.facebook.com/VestedTech/"
//         target="_blank"
//         rel="noopener noreferrer"
//     >
//         <Svg src="/images/general/Facebook.svg" />
//     </a>
//     <a
//         href="https://twitter.com/VestedHR"
//         target="_blank"
//         rel="noopener noreferrer"
//     >
//         <Svg src="/images/general/Twitter.svg" />
//     </a>
//     <a
//         href="https://www.linkedin.com/company/vestedtechnology"
//         target="_blank"
//         rel="noopener noreferrer"
//     >
//         <Svg src="/images/general/Linkedin.svg" />
//     </a>
// </div>
//             </div>
//         );
//     }
// }

class Links extends React.Component {
    render() {
        return (
            <div className="show-grid">
                <Row className="row-wrap">
                    <Col xs={12} sm={6} md={3}>
                        <h2 className="footer-heading">About Vested Technology</h2>
                        <Link to="/job-seekers" className="upper">Job Seekers</Link>
                        <Link to="/services" className="upper">Services</Link>
                        <a href="https://getvested.io/blog" target="_blank" className="upper" rel="noopener noreferrer">blog</a>
                        <Link to="/joinus" className="upper">Careers</Link>
                        <a className="upper" onClick={() => this.props.contactToggle()}>contact us</a>
                        <div className="social">
                            <a
                                href="https://www.facebook.com/VestedTech/"
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                <Svg src="/images/general/Facebook.svg" />
                            </a>
                            <a
                                href="https://twitter.com/VestedHR"
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                <Svg src="/images/general/Twitter.svg" />
                            </a>
                            <a
                                href="https://www.linkedin.com/company/vestedtechnology"
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                <Svg src="/images/general/Linkedin.svg" />
                            </a>
                        </div>

                    </Col>


                    <Col xs={12} sm={6} md={5}>
                        <h2 className="footer-heading">For Job Seekers</h2>
                        <a href="https://getvested.io/blog/accounting-jobs-nyc/" rel="noopener noreferrer" target="_blank">Career Guide to Accounting Jobs in NYC</a>
                        <a href="https://getvested.io/blog/ultimate-guide-to-careers-in-finance/" target="_blank" rel="noopener noreferrer">Career Guide to Finance</a>
                        <a href="https://getvested.io/temp-agency/job-seekers/" target="_blank" rel="noopener noreferrer">How our Temp Agency can help you break into an industry</a>
                        <a href="https://getvested.io/careers/accounting/accounting-manager/" target="_blank" rel="noopener noreferrer">Salary and Job Outlook – Accounting Manager</a>
                        <a href="https://getvested.io/careers/accounting/financial-controller/" target="_blank" rel="noopener noreferrer">Salary and Job Outlook – Financial Controller</a>
                        <a href="https://getvested.io/careers/finance/cfo/" target="_blank" rel="noopener noreferrer">Salary and Job Outlook – CFO</a>
                    </Col>
                    <Col xs={12} sm={6} md={4}>
                        <h2 className="footer-heading">For Employers</h2>
                        <a href="https://getvested.io/blog/hiring-temp-or-fulltime-workers/" target="_blank" rel="noopener noreferrer">Full Time Employees versus Temp Workers</a>
                        <a href="https://getvested.io/blog/recruitment-trends-hrtech/" target="_blank" rel="noopener noreferrer">2018 Recruitment Trends in HRTech</a>
                        <a href="https://getvested.io/temp-agency/hiring-solutions/" target="_blank" rel="noopener noreferrer">How to hire the best temp workers for your business</a>

                    </Col>
                </Row>
                <Row className="row-wrap" style={{ padding: 0 }}>
                    <Col xs={12} sm={6} md={6} className="contact">
                        <h1 className="footer-heading">Visit Us At</h1>
                        <a href="https://www.google.com/maps?ll=40.713409,-74.011926&z=13&t=m&hl=en-US&gl=US&mapclient=embed&cid=16092976047222606788" className="contact-text" style={{ display: 'inline-block' }} target="_blank" rel="noopener noreferrer"><Svg src="/images/general/location.svg" /> 250 Greenwich St. 46th FL, New York, NY 10007</a>
                        <div className="contact-text"><Svg src="/images/general/contact-number.svg" />(347) 618-0752</div>
                    </Col>
                    <Col md={2} />
                    <Col xs={12} sm={6} md={4} style={{ paddingBottom: '20px' }}>
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12096.837157465523!2d-74.011926!3d40.713409!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xdf55bc736192cbc4!2sVested+Technology!5e0!3m2!1sen!2sus!4v1529949380724" frameBorder="0" style={{ border: 0, width: "100%" }} title="vestedLocation" allowFullScreen></iframe>
                    </Col>
                </Row>
                {/* <Row className="row-wrap" style={{ paddingTop: 0 }}>
                    <Col xs={12} sm={6} md={4}>
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12096.837157465523!2d-74.011926!3d40.713409!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xdf55bc736192cbc4!2sVested+Technology!5e0!3m2!1sen!2sus!4v1529949380724" frameBorder="0" style={{ border: 0, width: "100%", height: "100%" }} title="vestedLocation" allowFullScreen></iframe>
                    </Col>
                </Row> */}
            </div>
        );
    }
}

class Policy extends React.Component {
    render() {
        return (
            <div className="policy">
                <Row className="row-wrap">
                    <Col xs={7} sm={8} md={6} className="col">
                        <Link to="/"><img src="/images/general/logo.svg" alt="getvested" /></Link>
                        <Link to="/terms-of-service">
                            <span>Terms and Conditions</span>
                        </Link>
                        <Link to="/privacy-policy">
                            <span>Privacy Policy</span>
                        </Link>
                    </Col>
                    <Col xs={5} sm={4} md={6} className="col">
                        <div className="copyright">
                            <span className="copyright-icon">©</span> Copyright Vested Technology 2018
                        </div>
                    </Col>
                </Row>
            </div>

        );
    }
}
