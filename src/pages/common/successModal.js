import React, { Fragment } from "react";
import { Modal, Row, Col, Button } from "react-bootstrap";

const FormSuccess = ({ formSendSuccess, successPopup, join }) => (
    <Modal show={formSendSuccess} bsSize="small" className="success-modal">
        <Modal.Body>
            <Row>
                <Col xs={12} sm={12} md={12} className="message">
                    {join ?

                        <p>Thanks for joining our talent community</p>

                        : <Fragment>
                            <p>We have received you query</p>
                            <p>Will get back to you soon</p>
                        </Fragment>}
                </Col>
            </Row>
            <Row>
                <Col xs={12} sm={12} md={12} className="btn-wrap">
                    <Button onClick={() => successPopup()} className="cancel">Close</Button>
                </Col>
            </Row>
        </Modal.Body>
    </Modal>
)

export default FormSuccess;