import React from "react";
import { Row, Grid, Button } from "react-bootstrap";
import {
    Link as ScrollLink
} from 'react-scroll'

const Splash = ({
    custClass,
    style,
    heading,
    content,
    buttonLink,
    btnStyle,
    buttonText,
    bgImg,
    scrollId,
    ...props
}) => ({
    render() {
        return (
            <section className={"splash " + custClass} style={{ style, backgroundImage: `url(${bgImg})` }} >
                <Grid fluid>
                    <Row>
                        {heading && <h3>{heading}</h3>}
                        {content && <p>{content}</p>}
                        {buttonLink ? (
                            <a
                                href={buttonLink}
                                className={
                                    btnStyle
                                        ? "btn btn-" + btnStyle
                                        : "btn btn-default"
                                }
                            >
                                {buttonText}
                            </a>
                        ) : buttonText ? (
                            <ScrollLink className={
                                btnStyle
                                    ? "btn btn-" + btnStyle
                                    : "btn btn-default"
                            }
                                to={scrollId} spy={true} smooth={true} duration={500} offset={-80}>
                                {buttonText}
                            </ScrollLink>
                ) : ''}
                    </Row>
                </Grid>
            </section>
        );
    }
});

export default Splash;
