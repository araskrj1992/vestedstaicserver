import React from "react";
// import { Link } from "react-router-dom";
import { Row, Grid, Col } from "react-bootstrap";
import Svg from "./svg";

const WhatWho = () => ({
    render() {
        return (
            <section
                id={this.props.id}
                className={
                    this.props.dark ? "whatWeBelieve dark" : "whatWeBelieve"
                }
            >
                <Grid>
                    <Row>
                        <Col
                            xs={12}
                            md={10}
                            mdPush={1}
                            className="text-center pl-5 pr-5"
                        >
                            <Svg
                                src="/images/idea.svg"
                            />
                            {this.props.heading && (
                                <h2 className="heading">
                                    {this.props.heading}
                                </h2>
                            )}
                            {this.props.text && <p>{this.props.text}</p>}
                        </Col>
                    </Row>
                </Grid>
            </section>
        );
    }
});

export default WhatWho;
