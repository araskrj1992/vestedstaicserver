import React from "react";
import { Modal, Row, Col, ControlLabel, FormControl, HelpBlock, Button } from "react-bootstrap";
import validator from 'validator'
import { makeRequest } from '../../helper'

export default class JoinForm extends React.Component {

    state = {
        name: "",
        email: "",
        linkedin: "",
        error: {
            name: false,
            email: false
        },
        isLoading: false,
        serverError: null
    }

    onSubmit = async (e) => {
        e.preventDefault()

        const { name, email, linkedin, error } = this.state
        let data = JSON.stringify({
            name: name.trim(),
            email: email.trim(),
            linkedin: linkedin.trim()
        })
        Object.keys(this.state.error).forEach(item => {
            if (item === 'name') {
                if (validator.isEmpty(this.state[item].trim())) {
                    this.setError('name', true)
                } else {
                    this.setError('name', false)
                }
            }
            if (item === 'email') {
                if (!validator.isEmail(this.state[item].trim())) {
                    this.setError('email', true)
                } else {
                    this.setError('email', false)
                }
            }
        })
        if (!error.name && !error.email && !error.query) {
            this.setState({ isLoading: true })
            try {
                let fetchResult = await makeRequest('POST', '/joincommunity', data)
                fetchResult = JSON.parse(fetchResult)
                if (fetchResult.success) {
                    this.setState({
                        isLoading: false,
                        name: "",
                        email: "",
                        linkedin: ""
                    })
                    this.props.contactToggle()
                    this.props.successPopup()
                }
            } catch (err) {
                this.setState({ isLoading: false, serverError: "There is some server error" })
            }

        }
    }

    setError(key, val) {
        let error = this.state.error
        error[key] = val
        this.setState({
            error
        })
    }

    handleChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    render() {
        const { serverError, isLoading } = this.state
        return (
            <Modal show={this.props.contact} className="contact-form">
                <Modal.Header className="form-header" >
                    <Modal.Title>Join <span>Us</span></Modal.Title>
                </Modal.Header>
                <Modal.Body className="form-content">
                    <form onSubmit={this.onSubmit}>
                        <Row>
                            <Col xs={12} sm={12} md={12} >
                                {serverError && <span style={{ color: 'red', paddingBottom: '20px' }}>{serverError}</span>}
                            </Col>
                            <Col xs={12} sm={6} md={6} className="input-wrap">
                                <ControlLabel>Name<span className="required">*</span></ControlLabel>
                                <FormControl
                                    type="text"
                                    name="name"
                                    placeholder="Enter Name"
                                    value={this.state.name}
                                    onChange={(e) => this.handleChange(e)}
                                />
                                {this.state.error.name && <HelpBlock className="error">Name is required</HelpBlock>}
                            </Col>
                            <Col xs={12} sm={6} md={6} className="input-wrap">
                                <ControlLabel>Email<span className="required">*</span></ControlLabel>
                                <FormControl
                                    type="email"
                                    name="email"
                                    placeholder="Enter Email"
                                    value={this.state.email}
                                    onChange={(e) => this.handleChange(e)}
                                />
                                {this.state.error.email && <HelpBlock className="error">Email is required with valid email</HelpBlock>}
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={12} sm={12} md={12} className="input-wrap">
                                <ControlLabel>Linkedin</ControlLabel>
                                <FormControl
                                    type="text"
                                    name="linkedin"
                                    placeholder="Linkedin"
                                    value={this.state.linkedin}
                                    onChange={(e) => this.handleChange(e)}
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={12} sm={12} md={12} className="btn-wrap">
                                {!isLoading && <Button className="submit-btn cancel" onClick={() => this.props.contactToggle()}>Cancel</Button>}
                                {!isLoading ? <Button className="submit-btn" type="submit" >Submit</Button> : <Button className="submit-btn" disabled={true} >Sending Query</Button>}
                            </Col>
                        </Row>
                    </form>
                </Modal.Body>
            </Modal>
        );
    }
}