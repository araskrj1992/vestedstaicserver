import React from "react";
import { Row, Col, PanelGroup, Panel } from "react-bootstrap";
import Svg from "./svg";


const FrequentQuestions = ({ questions }) => {
    return (
        <section className="frequentQuestions">
            <Row>
                <Col xs={12} md={12} className="text-center">
                    <h2 className="heading">Frequently Asked Questions</h2>
                </Col>
            </Row>
            <Row className="content-wrapper">
                <PanelGroup accordion id="accordion-example">
                    {
                        questions.map((item, i) => {
                            return (
                                <Panel eventKey={i + 1} key={i}>
                                    <Panel.Heading>
                                        <Panel.Title toggle>{item.title}
                                            <Svg src="/images/careers/toggle-arrow.svg" />
                                        </Panel.Title>
                                    </Panel.Heading>
                                    <Panel.Body collapsible>{item.answer}</Panel.Body>
                                </Panel>
                            )
                        })
                    }
                </PanelGroup>
            </Row>
        </section>
    );
}


export default FrequentQuestions;
