import React from "react";
import Svg from "./svg"
import PolicyTerms from "./policyTerms"

const BrandFooter = () => ({
    render() {
        return (
            <footer className="footer brandFooter">
                <div className="social-content-wrapper">
                    <div>
                        <h5>Powered By <img src="/images/brand/vested.png" alt="vested" /></h5>
                    </div>
                    <div>
                        <a
                            href="https://www.facebook.com/VestedTech/"
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            <Svg src="/images/general/Facebook.svg" />
                        </a>
                        <a
                            href="https://twitter.com/VestedHR"
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            <Svg src="/images/general/Twitter.svg" />
                        </a>
                        <a
                            href="https://www.linkedin.com/company/vestedtechnology"
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            <Svg src="/images/general/Linkedin.svg" />
                        </a>
                    </div>
                </div>
                <PolicyTerms brand />
            </footer>
        );
    }
});

export default BrandFooter;
