import '@babel/polyfill'
import express from 'express'
import bodyParser from 'body-parser'
import React from 'react'
import ReactDOMServer from 'react-dom/server'
import { StaticRouter } from 'react-router-dom'
import { Helmet } from 'react-helmet'
import App from './src/app'
import ContextProvider from './src/ContextProvider'
const mainController = require("./server/controllers/main");
const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static('public'))


app.use(function (req, res, next) {
    var contentType = req.headers['content-type'];
    if (req.method === 'POST' && (!contentType || contentType.indexOf('application/json') === -1)) {
        return res.sendStatus(400);
    }
    next();
});


app.post('/send', mainController.contact)
app.post('/sendPricingScheme', mainController.sendPricingScheme)
app.post('/joincommunity', mainController.joinCommunity)
app.post('/upgradePackage', mainController.upgradePackage)


app.get('*', (req, res) => {

    const css = new Set()
    const context = {
        insertCss: (...styles) =>
            styles.forEach(style => css.add(style._getCss()))
    }
    const content = ReactDOMServer.renderToString(
        <StaticRouter location={req.url} context={context}>
            <ContextProvider context={context}>
                <App />
            </ContextProvider>
        </StaticRouter>
    )

    const helmet = Helmet.renderStatic()
    const html = `
        <html>
            <head>
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width">
                <meta name="viewport" content="width=device-width,user-scalable=no">
                <link rel="manifest" href="/favicons/manifest.json">
                <link rel="shortcut icon" href="/favicons/favicon.ico">
                <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
                <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
                ${helmet.meta.toString()}
                ${helmet.title.toString()}
                <style type="text/css">${[...css].join('')}</style>
                <script>
                    (function (i, s, o, g, r, a, m) {
                    i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                        (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date(); a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
                    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
                    ga('create', "UA-99846828-1", 'auto');
                    ga('send', 'pageview');

                </script>
            </head>
            <body>
                <div id="app">${content}</div>
                <script src="/client_bundle.js"></script>
                 <!-- Start of HubSpot Embed Code -->
                <script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/3810360.js"></script>
                <!-- End of HubSpot Embed Code -->
            </body>
        </html>
    `
    res.send(html)
})


const PORT = process.env.PORT || 9000;

app.listen(PORT, () => {
    console.log(`Server is running on ${PORT}`)
})