const express = require('express');
const router = express.Router();
const nodemailer = require('nodemailer');
const fs = require('fs');

// create reusable transporter object using the default SMTP transport
// let transporter = nodemailer.createTransport({
//     host: "email-smtp.us-east-1.amazonaws.com",
//     port: 587,
//     secureConnection: false,
//     auth: {
//         user: "AKIAJQFF4TARDNIQSIEQ",
//         pass: "AoT8DMDrEkmFFL9lE+Pg1ri5xq0fNjBBcePcTExiioYW"
//     },
//     tls: {
//         ciphers: "SSLv3"
//     }
// });


let transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'VestedContactUs@gmail.com', // generated ethereal user
        pass: '3Vested33'  // generated ethereal password
    }
});



exports.contact = (req, res) => {
    const body = req.body;
    const output = `
    <p>You have a new query</p>
    <h3>Information of query</h3>
    <ul style="list-style: circle;">  
      <li>Name: ` + body.name + `</li>
      <li>Email: ` + body.email + `</li>
      <li>Company Name:` + body.company + `</li>
      <li>Position:` + body.position + `</li>
    </ul>
    <h3>Message</h3>
    <p>` + body.query + `</p>
  `;

    // setup email data with unicode symbols
    let mailOptions = {
        from: '"Query" <VestedContactUs@gmail.com>', // sender address
        to: 'akash@getvested.io', // list of receivers
        cc: 'andy@getvested.io',
        subject: 'Query for Vested', // Subject line
        html: output // html body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            console.log(error)
            return res.json({ errror: error });
        }
        res.json({ message: 'message sent', success: 200 });
    });
}


exports.sendPricingScheme = (req, res) => {
    const body = req.body;
    const output = `
    <p>You have a new query</p>
    <h3>Information of query</h3>
    <ul style="list-style: circle;">  
      <li>Name: ` + body.name + `</li>
      <li>Email: ` + body.email + `</li>
      <li>Company Name:` + body.company + `</li>
      <li>Position:` + body.position + `</li>
    </ul>
    <h3>Message</h3>
    <p>` + body.jobDescription + `</p>
    <p>` + body.otherDescription + `</p>
    <p>` + body.pricingQuery + `</p>
  `;

    // setup email data with unicode symbols
    let mailOptions = {
        from: '"Scheme Query" <VestedContactUs@gmail.com>', // sender address
        to: 'akash@getvested.io', // list of receivers
        cc: 'andy@getvested.io',
        subject: 'Scheme query for Vested', // Subject line
        html: output, // html body
    };

    if (body.uploadfileName) {
        mailOptions['attachments'] = [
            {   // file on disk as an attachment
                filename: body.uploadfileName,
                path: body.uploadfilePath
            }
        ]
    }

    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            console.log(error)
            return res.json({ errror: error });
        }
        res.json({ message: 'message sent', success: 200 });
    });
}


exports.joinCommunity = (req, res) => {
    const body = req.body;
    const output = `
    <p>You have a new member</p>
    <h3>Wantto join community</h3>
    <ul style="list-style: circle;">  
      <li>Name: ` + body.name + `</li>
      <li>Email: ` + body.email + `</li>
      <li>Linkedin:` + body.linkedin + `</li>
    </ul>
  `;

    // setup email data with unicode symbols
    let mailOptions = {
        from: '"Join Vested" <VestedContactUs@gmail.com>', // sender address
        to: 'akash@getvested.io', // list of receivers
        cc: 'andy@getvested.io',
        subject: 'Join Vested Community', // Subject line
        html: output // html body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            console.log(error)
            return res.json({ errror: error });
        }
        res.json({ message: 'message sent', success: 200 });
    });
}

exports.upgradePackage = (req, res) => {
    const body = req.body;
    const output = `
    <p>`+ body.msg + `</p>
  `;

    // setup email data with unicode symbols
    let mailOptions = {
        from: 'Upgrade request <VestedContactUs@gmail.com>', // sender address
        to: 'ankit@getvested.io', // list of receivers
        // cc: 'andy@getvested.io',
        subject: 'Client want to upgrade their package', // Subject line
        html: output // html body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            console.log(error)
            return res.json({ errror: error });
        }
        res.json({ message: 'message sent', success: 200 });
    });
}

