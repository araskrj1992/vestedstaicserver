require('babel-register')({
    presets: ['env']
})


global.window = {
    location: { origin: "https://getvested.io" },
    navigator: { userAgent: "" },
    server: true,
    addEventListener: () => { }
};
document = { createElement: function () { return {} } }

// Import the rest of our application.
module.exports = require('./server.js')
